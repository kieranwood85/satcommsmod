/*	
	Iridium SBD communication library for Arduino
	Created by Samuel P. Pownall, January 2017
*/

#include "Arduino.h"
#include "Iridium.h"
#include "Stream.h"

int Iridium::update() {
	
	int response;
	char message[340];
	memset(message, '\0', sizeof(message));
	
	if (state.SBDRB <= 1) getMessage(message);
	
	elapsedTime = millis() - initialTime;
	
	switch(currentJob) {
		
		case NONE:
			response = IDLE;
		break;
		
		case INIT:
			response = internalINIT(message);
			if (response != BUSY) state.INIT = 0;
		break;
		
		case CSQ:
			response = internalCSQ(message);
			if (response != BUSY) state.CSQ = 0;
		break;
		
		case SBDWB:
			response = internalSBDWB(message);
			if (response != BUSY) state.SBDWB = 0;
		break;
		
		case SBDRB:
			response = internalSBDRB(message);
			if (response != BUSY) state.SBDRB = 0;
		break;
		
		case SBDIX:
			response = internalSBDIX(message);
			if (response != BUSY) state.SBDIX = 0;
		break;
		
		case SBDIXA:
			response = internalSBDIXA(message);
			if (response != BUSY) state.SBDIXA = 0;
		break;
		
		case SBDD:
			response = internalSBDD(message);
			if (response != BUSY) state.SBDD = 0;
		break;
		
		case SBDC:
			response = internalSBDC(message);
			if (response != BUSY) state.SBDC = 0;
		break;
		
		case MSSTM:
			response = internalMSSTM(message);
			if (response != BUSY) state.MSSTM = 0;
		break;
		
		case SBDTC:
			response = internalSBDTC(message);
			if (response != BUSY) state.SBDTC = 0;
		break;
		
		default:
		response = FAILURE;
		break;
		
	}
	
	if (response != BUSY) {
		currentJob = NONE;
		busy = false;
	}
	
	return response;
	
}

int Iridium::getMessage(char *message) {
	
	while(stream.available() > 0) {
		if ((streamBuffer[bufferIndex] = stream.read()) == '\r') {
			
			if (streamBuffer[0] != '\n' && streamBuffer[0] != '\r') {
				Serial.print(">>");
				Serial.write(streamBuffer, bufferIndex);
				Serial.println();
			}
			
			memcpy(message, streamBuffer, bufferIndex + 1);
			memset(streamBuffer, '\0', sizeof(streamBuffer));
			bufferIndex = 0;
			return SUCCESS;
			
		} else if (streamBuffer[bufferIndex] == '\n') {
			
			streamBuffer[bufferIndex] = '\0';
			
		} else {
			
			bufferIndex++;
			
			if (bufferIndex == sizeof(streamBuffer)) {
				
				memset(streamBuffer, '\0', sizeof(streamBuffer));
				bufferIndex = 0;
				return FAILURE;
				
			}
		}
	}
	
	return BUSY;
	
}

int Iridium::doINIT() {
	
	if(busy) return BUSY;
	currentJob = INIT;
	state.INIT = 0;
	beginTimer();
	busy = true;
	
	return SUCCESS;
	
}

int Iridium::doCSQ(int *signalStrength) {
	
	if(busy) return BUSY;
	currentJob = CSQ;
	state.CSQ = 0;
	beginTimer();
	busy = true;
	this->signalStrength = signalStrength;	
	return SUCCESS;
	
}

int Iridium::doSBDWB(uint8_t *txBuffer, int txLength) {
	
	if(busy) return BUSY;
	currentJob = SBDWB;
	state.SBDWB = 0;
	beginTimer();
	busy = true;
	this->txBuffer = txBuffer;
	this->txLength = txLength;
	return SUCCESS;
	
}

int Iridium::doSBDRB(uint8_t *rxBuffer, int *rxLength) {
	
	if(busy) return BUSY;
	currentJob = SBDRB;
	state.SBDRB = 0;
	beginTimer();
	busy = true;
	this->rxBuffer = rxBuffer;
	this->rxLength = rxLength;
	memset(rxBuffer, '\0', *rxLength);
	return SUCCESS;
	
}

int Iridium::doSBDIX() {
	
	if(busy) return BUSY;
	currentJob = SBDIX;
	state.SBDIX = 0;
	beginTimer();
	busy = true;
	
	return SUCCESS;
	
}

int Iridium::doSBDIXA() {
	
	if(busy) return BUSY;
	currentJob = SBDIXA;
	state.SBDIXA = 0;
	beginTimer();
	busy = true;
	
	return SUCCESS;
	
}

int Iridium::doSBDD(int deleteType) {
	
	if(busy) return BUSY;
	currentJob = SBDD;
	state.SBDD = 0;
	beginTimer();
	busy = true;
	this->deleteType = deleteType;
	
	return SUCCESS;
	
}

int Iridium::doSBDC() {
	
	if(busy) return BUSY;
	currentJob = SBDC;
	state.SBDC = 0;
	beginTimer();
	busy = true;
	
	return SUCCESS;
	
}

int Iridium::doMSSTM() {
	
	if(busy) return BUSY;
	currentJob = MSSTM;
	state.MSSTM = 0;
	beginTimer();
	busy = true;
	
	return SUCCESS;
	
}

int Iridium::doSBDTC() {
	
	if(busy) return BUSY;
	currentJob = SBDTC;
	state.SBDTC = 0;
	beginTimer();
	busy = true;
	
	return SUCCESS;
	
}

int Iridium::internalINIT(char *message) {
	
	if (elapsedTime > timeout.INIT) return FAILURE;

	return SUCCESS;
	
}

int Iridium::internalCSQ(char *message) {
	
	switch (state.CSQ) {
		
		case 0:
			stream.print("AT+CSQ\r");
			state.CSQ = 1;
		break;
		
		case 1:
			if (strstr(message, "ERROR\r") != NULL) return FAILURE;
			if (sscanf(message,"+CSQ:%d\r",signalStrength) == 1) state.CSQ = 2;
		break;
		
		case 2:
			if (strstr(message, "OK\r") != NULL) return SUCCESS;
		break;
		
	}
	
	if (elapsedTime > timeout.CSQ) return FAILURE;
	return BUSY;
	
}

int Iridium::internalSBDWB(char *message) {
	
	switch(state.SBDWB) {
		
		case 0:
			stream.print("AT+SBDWB=");
			stream.print(txLength);
			stream.print("\r");
			state.SBDWB = 1;
		break;
		
		case 1:
			if (strstr(message, "ERROR\r") != NULL) return FAILURE;
			if (strstr(message, "READY\r") != NULL) state.SBDWB = 2;
		break;
		
		case 2:
			generateChecksum();
			stream.write(txBuffer, txLength);
			state.SBDWB = 3;
		break;
		
		case 3:
			if (strstr(message, "0\r") != NULL) return SUCCESS;
			if (strstr(message, "1\r") != NULL || strstr(message, "2\r") != NULL || strstr(message, "3\r") != NULL) return FAILURE;
		break;
		
	}
	
	if (elapsedTime > timeout.SBDWB) return FAILURE;
	return BUSY;
	
}

int Iridium::internalSBDRB(char *message) {
	
	switch(state.SBDRB) {
		
		case 0:
			stream.print("AT+SBDRB\r");
			state.SBDRB = 1;
		break;
		
		case 1:
			if (strstr(message, "ERROR\r") != NULL) return FAILURE;
			if (strstr(message, "AT+SBDRB\r") != NULL) state.SBDRB = 2;
		break;
		
		case 2:
			if (stream.available() >= 2) {
				size = 256 * stream.read() + stream.read();
				binaryIndex = 0;
				binarySum = 0;
				state.SBDRB = 3;
			}
		break;
		
		case 3:
			while (stream.available() > 0 && binaryIndex != size) {
				rxBuffer[binaryIndex] = stream.read();
				binarySum += rxBuffer[binaryIndex];
				binaryIndex++;
			}
			if (binaryIndex == size) state.SBDRB = 4;
		break;
		
		case 4:
			if (stream.available() >= 2) {
				uint16_t checksum = 256 * stream.read() + stream.read();
				if (checksum == binarySum) {
					*rxLength = size;
					return SUCCESS;
				} else {
					return FAILURE;
				}
			}
		break;
		
	}
	
	if (elapsedTime > timeout.SBDRB) return FAILURE;
	return BUSY;
	
}

int Iridium::internalSBDIX(char *message) {
	
	int moStatus = -1, momsn = -1, mtStatus = -1, mtmsn = -1, mtLength = -1, mtQueued = -1;
	
	switch(state.SBDIX) {
		
		case 0:
			stream.print("AT+SBDIX\r");
			state.SBDIX = 1;
		break;
		
		case 1:
			if (strstr(message, "ERROR\r") != NULL) return FAILURE;
			if (sscanf(message, "+SBDIX:%d,%d,%d,%d,%d,%d\r", &moStatus, &momsn, &mtStatus, &mtmsn, &mtLength, &mtQueued) == 6) {
				if (moStatus == 0) return SUCCESS;
				else return FAILURE;
			}
		break;
		
	}
	
	if (elapsedTime > timeout.SBDIX) return FAILURE;
	return BUSY;
	
}

int Iridium::internalSBDIXA(char *message) {
	
	if (elapsedTime > timeout.SBDIXA) return FAILURE;
	
	return SUCCESS;
	
}

int Iridium::internalSBDD(char *message) {
	
	switch(state.SBDD) {
		
		case 0:
			stream.print("AT+SBDD");
			stream.print(deleteType);
			stream.print("\r");
			state.SBDD = 1;
		break;
		
		case 1:
			if (strstr(message, "0\r") != NULL) return SUCCESS;
			if (strstr(message, "1\r") != NULL || strstr(message, "ERROR\r") != NULL) return FAILURE;
		break;
		
	}
	
	if (elapsedTime > timeout.SBDD) return FAILURE;
	return BUSY;
	
}

int Iridium::internalSBDC(char *message) {
	
	if (elapsedTime > timeout.SBDC) return FAILURE;
	
	return SUCCESS;
	
}

int Iridium::internalMSSTM(char *message) {
	
	if (elapsedTime > timeout.MSSTM) return FAILURE;
	
	return SUCCESS;
	
}

int Iridium::internalSBDTC(char *message) {
	
	switch(state.SBDTC) {
		
		case 0:
			stream.print("AT+SBDTC\r");
			state.SBDTC = 1;
		break;
		
		case 1:
			if (strstr(message, "ERROR\r") != NULL) return FAILURE;
			if (sscanf(message, "SBDTC: Outbound SBD Copied to Inbound SBD: size = %d\r",&junk) == 1) return SUCCESS;
		break;
		
	}
	
	if (elapsedTime > timeout.SBDTC) return FAILURE;
	return BUSY;
	
}

void Iridium::generateChecksum() {
	
	uint16_t sum = 0;
	
	for (int i = 0; i < txLength; i++) {
		
		sum += txBuffer[i];
		
	}
	
	uint8_t lowerByte = (uint8_t) sum;
	uint8_t upperByte = (uint8_t) (sum >> 8);
	
	txBuffer[txLength] = upperByte;
	txBuffer[txLength + 1] = lowerByte;
	txLength += 2;
	
}

void Iridium::beginTimer() {
	elapsedTime = 0;
	initialTime = millis();
}
