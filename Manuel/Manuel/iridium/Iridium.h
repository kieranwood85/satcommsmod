/*	
	Iridium SBD communication library for Arduino
	Created by Samuel P. Pownall, January 2017
*/

#ifndef Iridium_h
#define Iridium_h

#include "Arduino.h"
#include "Stream.h"

class Iridium {
	
	private:
	
		enum CURRENTJOB {NONE, INIT, CSQ, SBDWB, SBDRB, SBDIX, SBDIXA, SBDD, SBDC, MSSTM, SBDTC};
		
		Stream &stream;
		char streamBuffer[340];
		int bufferIndex = 0, binaryIndex = 0;
		
		int junk = 0, size = 0, binarySum = 0;
		
		int txLength = 0;
		int *signalStrength = NULL, *rxLength = NULL;
		uint8_t *txBuffer, *rxBuffer;
		
		unsigned long initialTime = 0;
		unsigned long elapsedTime = 0;
		
		struct time_out {
			unsigned long IDLE = 1000;
			unsigned long INIT = 1000;
			unsigned long CSQ = 30000;
			unsigned long SBDWB = 10000;
			unsigned long SBDRB = 1000;
			unsigned long SBDIX = 60000;
			unsigned long SBDIXA = 1000;
			unsigned long SBDD = 2000;
			unsigned long SBDC = 1000;
			unsigned long MSSTM = 1000;		
			unsigned long SBDTC = 20000;
		} timeout;
		
		struct function_state {
			int IDLE = 0;
			int INIT = 0;
			int CSQ = 0;
			int SBDWB = 0;
			int SBDRB = 0;
			int SBDIX = 0;
			int SBDIXA = 0;
			int SBDD = 0;
			int SBDC = 0;
			int MSSTM = 0;
			int SBDTC = 0;
		} state;
		
		int powerPin, deleteType, currentJob = NONE;
		bool busy;
		
		int internalINIT(char *message);
		int internalCSQ(char *message);
		int internalSBDWB(char *message);
		int internalSBDRB(char *message);
		int internalSBDIX(char *message);
		int internalSBDIXA(char *message);
		int internalSBDD(char *message);
		int internalSBDC(char *message);
		int internalMSSTM(char *message);
		int internalSBDTC(char *message);
		
		int getMessage(char *message);
		
		void generateChecksum();
		void beginTimer();
		
	public:
	
		enum RESPONSE {SUCCESS, FAILURE, BUSY, IDLE};
		
		Iridium(Stream &str, int sleepPin):stream(str),powerPin(sleepPin),currentJob(IDLE),busy(false){}
		
		int update();
		
		int doINIT();
		int doCSQ(int *signalStrength);
		int doSBDWB(uint8_t *txBuffer, int txLength);
		int doSBDRB(uint8_t *rxBuffer, int *rxLength);
		int doSBDIX();
		int doSBDIXA();
		int doSBDD(int deleteType);
		int doSBDC();
		int doMSSTM();
		int doSBDTC();
	
};

#endif
	