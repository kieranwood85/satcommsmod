#include "mavlink\common\mavlink.h"

void setup() {

  Serial1.begin(19200);
  Serial.begin(9600);

}

void loop() {

  mavlinkReceive();

}

void mavlinkReceive() {

  mavlink_message_t msg;
  mavlink_status_t sts;

  Serial.print("MAVLINK: ");

  while(Serial1.available() > 0) {

    uint8_t c = Serial1.read();
    if (c < 16) Serial.print(0);
    Serial.print(c,HEX);
    
  }

  Serial.println();
  
}

