#pragma once
// MESSAGE SATCOMM_TELEMETRY PACKING

#define MAVLINK_MSG_ID_SATCOMM_TELEMETRY 180

MAVPACKED(
typedef struct __mavlink_satcomm_telemetry_t {
 uint32_t time; /*< Description of the field*/
 int32_t latitude; /*< Description of the field*/
 int32_t longitude; /*< Description of the field*/
 float altitude; /*< Description of the field*/
 float nav_distance; /*< Description of the field*/
 uint32_t health; /*< Description of the field*/
 uint16_t nav_bearing; /*< Description of the field*/
 uint16_t ground_speed; /*< Description of the field*/
 uint16_t air_speed; /*< Description of the field*/
 uint16_t bat_voltage; /*< Description of the field*/
 uint16_t bat_current; /*< Description of the field*/
 int16_t roll; /*< Description of the field*/
 int16_t pitch; /*< Description of the field*/
 int16_t wind_speed; /*< Description of the field*/
 int16_t wind_dir; /*< Description of the field*/
 int16_t waypoint_num; /*< Description of the field*/
 int16_t throttle; /*< Description of the field*/
 int16_t vertical_speed; /*< Description of the field*/
 uint8_t mode; /*< Description of the field*/
 uint8_t gps_nsats; /*< Description of the field*/
 uint8_t gps_hdop; /*< Description of the field*/
 uint8_t gps_fixtype; /*< Description of the field*/
 uint8_t bat_percent; /*< Description of the field*/
}) mavlink_satcomm_telemetry_t;

#define MAVLINK_MSG_ID_SATCOMM_TELEMETRY_LEN 53
#define MAVLINK_MSG_ID_SATCOMM_TELEMETRY_MIN_LEN 53
#define MAVLINK_MSG_ID_180_LEN 53
#define MAVLINK_MSG_ID_180_MIN_LEN 53

#define MAVLINK_MSG_ID_SATCOMM_TELEMETRY_CRC 252
#define MAVLINK_MSG_ID_180_CRC 252



#if MAVLINK_COMMAND_24BIT
#define MAVLINK_MESSAGE_INFO_SATCOMM_TELEMETRY { \
    180, \
    "SATCOMM_TELEMETRY", \
    23, \
    {  { "time", NULL, MAVLINK_TYPE_UINT32_T, 0, 0, offsetof(mavlink_satcomm_telemetry_t, time) }, \
         { "latitude", NULL, MAVLINK_TYPE_INT32_T, 0, 4, offsetof(mavlink_satcomm_telemetry_t, latitude) }, \
         { "longitude", NULL, MAVLINK_TYPE_INT32_T, 0, 8, offsetof(mavlink_satcomm_telemetry_t, longitude) }, \
         { "altitude", NULL, MAVLINK_TYPE_FLOAT, 0, 12, offsetof(mavlink_satcomm_telemetry_t, altitude) }, \
         { "nav_distance", NULL, MAVLINK_TYPE_FLOAT, 0, 16, offsetof(mavlink_satcomm_telemetry_t, nav_distance) }, \
         { "health", NULL, MAVLINK_TYPE_UINT32_T, 0, 20, offsetof(mavlink_satcomm_telemetry_t, health) }, \
         { "nav_bearing", NULL, MAVLINK_TYPE_UINT16_T, 0, 24, offsetof(mavlink_satcomm_telemetry_t, nav_bearing) }, \
         { "ground_speed", NULL, MAVLINK_TYPE_UINT16_T, 0, 26, offsetof(mavlink_satcomm_telemetry_t, ground_speed) }, \
         { "air_speed", NULL, MAVLINK_TYPE_UINT16_T, 0, 28, offsetof(mavlink_satcomm_telemetry_t, air_speed) }, \
         { "bat_voltage", NULL, MAVLINK_TYPE_UINT16_T, 0, 30, offsetof(mavlink_satcomm_telemetry_t, bat_voltage) }, \
         { "bat_current", NULL, MAVLINK_TYPE_UINT16_T, 0, 32, offsetof(mavlink_satcomm_telemetry_t, bat_current) }, \
         { "roll", NULL, MAVLINK_TYPE_INT16_T, 0, 34, offsetof(mavlink_satcomm_telemetry_t, roll) }, \
         { "pitch", NULL, MAVLINK_TYPE_INT16_T, 0, 36, offsetof(mavlink_satcomm_telemetry_t, pitch) }, \
         { "wind_speed", NULL, MAVLINK_TYPE_INT16_T, 0, 38, offsetof(mavlink_satcomm_telemetry_t, wind_speed) }, \
         { "wind_dir", NULL, MAVLINK_TYPE_INT16_T, 0, 40, offsetof(mavlink_satcomm_telemetry_t, wind_dir) }, \
         { "waypoint_num", NULL, MAVLINK_TYPE_INT16_T, 0, 42, offsetof(mavlink_satcomm_telemetry_t, waypoint_num) }, \
         { "throttle", NULL, MAVLINK_TYPE_INT16_T, 0, 44, offsetof(mavlink_satcomm_telemetry_t, throttle) }, \
         { "vertical_speed", NULL, MAVLINK_TYPE_INT16_T, 0, 46, offsetof(mavlink_satcomm_telemetry_t, vertical_speed) }, \
         { "mode", NULL, MAVLINK_TYPE_UINT8_T, 0, 48, offsetof(mavlink_satcomm_telemetry_t, mode) }, \
         { "gps_nsats", NULL, MAVLINK_TYPE_UINT8_T, 0, 49, offsetof(mavlink_satcomm_telemetry_t, gps_nsats) }, \
         { "gps_hdop", NULL, MAVLINK_TYPE_UINT8_T, 0, 50, offsetof(mavlink_satcomm_telemetry_t, gps_hdop) }, \
         { "gps_fixtype", NULL, MAVLINK_TYPE_UINT8_T, 0, 51, offsetof(mavlink_satcomm_telemetry_t, gps_fixtype) }, \
         { "bat_percent", NULL, MAVLINK_TYPE_UINT8_T, 0, 52, offsetof(mavlink_satcomm_telemetry_t, bat_percent) }, \
         } \
}
#else
#define MAVLINK_MESSAGE_INFO_SATCOMM_TELEMETRY { \
    "SATCOMM_TELEMETRY", \
    23, \
    {  { "time", NULL, MAVLINK_TYPE_UINT32_T, 0, 0, offsetof(mavlink_satcomm_telemetry_t, time) }, \
         { "latitude", NULL, MAVLINK_TYPE_INT32_T, 0, 4, offsetof(mavlink_satcomm_telemetry_t, latitude) }, \
         { "longitude", NULL, MAVLINK_TYPE_INT32_T, 0, 8, offsetof(mavlink_satcomm_telemetry_t, longitude) }, \
         { "altitude", NULL, MAVLINK_TYPE_FLOAT, 0, 12, offsetof(mavlink_satcomm_telemetry_t, altitude) }, \
         { "nav_distance", NULL, MAVLINK_TYPE_FLOAT, 0, 16, offsetof(mavlink_satcomm_telemetry_t, nav_distance) }, \
         { "health", NULL, MAVLINK_TYPE_UINT32_T, 0, 20, offsetof(mavlink_satcomm_telemetry_t, health) }, \
         { "nav_bearing", NULL, MAVLINK_TYPE_UINT16_T, 0, 24, offsetof(mavlink_satcomm_telemetry_t, nav_bearing) }, \
         { "ground_speed", NULL, MAVLINK_TYPE_UINT16_T, 0, 26, offsetof(mavlink_satcomm_telemetry_t, ground_speed) }, \
         { "air_speed", NULL, MAVLINK_TYPE_UINT16_T, 0, 28, offsetof(mavlink_satcomm_telemetry_t, air_speed) }, \
         { "bat_voltage", NULL, MAVLINK_TYPE_UINT16_T, 0, 30, offsetof(mavlink_satcomm_telemetry_t, bat_voltage) }, \
         { "bat_current", NULL, MAVLINK_TYPE_UINT16_T, 0, 32, offsetof(mavlink_satcomm_telemetry_t, bat_current) }, \
         { "roll", NULL, MAVLINK_TYPE_INT16_T, 0, 34, offsetof(mavlink_satcomm_telemetry_t, roll) }, \
         { "pitch", NULL, MAVLINK_TYPE_INT16_T, 0, 36, offsetof(mavlink_satcomm_telemetry_t, pitch) }, \
         { "wind_speed", NULL, MAVLINK_TYPE_INT16_T, 0, 38, offsetof(mavlink_satcomm_telemetry_t, wind_speed) }, \
         { "wind_dir", NULL, MAVLINK_TYPE_INT16_T, 0, 40, offsetof(mavlink_satcomm_telemetry_t, wind_dir) }, \
         { "waypoint_num", NULL, MAVLINK_TYPE_INT16_T, 0, 42, offsetof(mavlink_satcomm_telemetry_t, waypoint_num) }, \
         { "throttle", NULL, MAVLINK_TYPE_INT16_T, 0, 44, offsetof(mavlink_satcomm_telemetry_t, throttle) }, \
         { "vertical_speed", NULL, MAVLINK_TYPE_INT16_T, 0, 46, offsetof(mavlink_satcomm_telemetry_t, vertical_speed) }, \
         { "mode", NULL, MAVLINK_TYPE_UINT8_T, 0, 48, offsetof(mavlink_satcomm_telemetry_t, mode) }, \
         { "gps_nsats", NULL, MAVLINK_TYPE_UINT8_T, 0, 49, offsetof(mavlink_satcomm_telemetry_t, gps_nsats) }, \
         { "gps_hdop", NULL, MAVLINK_TYPE_UINT8_T, 0, 50, offsetof(mavlink_satcomm_telemetry_t, gps_hdop) }, \
         { "gps_fixtype", NULL, MAVLINK_TYPE_UINT8_T, 0, 51, offsetof(mavlink_satcomm_telemetry_t, gps_fixtype) }, \
         { "bat_percent", NULL, MAVLINK_TYPE_UINT8_T, 0, 52, offsetof(mavlink_satcomm_telemetry_t, bat_percent) }, \
         } \
}
#endif

/**
 * @brief Pack a satcomm_telemetry message
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 *
 * @param time Description of the field
 * @param latitude Description of the field
 * @param longitude Description of the field
 * @param altitude Description of the field
 * @param mode Description of the field
 * @param nav_bearing Description of the field
 * @param nav_distance Description of the field
 * @param health Description of the field
 * @param ground_speed Description of the field
 * @param air_speed Description of the field
 * @param gps_nsats Description of the field
 * @param gps_hdop Description of the field
 * @param gps_fixtype Description of the field
 * @param bat_voltage Description of the field
 * @param bat_percent Description of the field
 * @param bat_current Description of the field
 * @param roll Description of the field
 * @param pitch Description of the field
 * @param wind_speed Description of the field
 * @param wind_dir Description of the field
 * @param waypoint_num Description of the field
 * @param throttle Description of the field
 * @param vertical_speed Description of the field
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_satcomm_telemetry_pack(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg,
                               uint32_t time, int32_t latitude, int32_t longitude, float altitude, uint8_t mode, uint16_t nav_bearing, float nav_distance, uint32_t health, uint16_t ground_speed, uint16_t air_speed, uint8_t gps_nsats, uint8_t gps_hdop, uint8_t gps_fixtype, uint16_t bat_voltage, uint8_t bat_percent, uint16_t bat_current, int16_t roll, int16_t pitch, int16_t wind_speed, int16_t wind_dir, int16_t waypoint_num, int16_t throttle, int16_t vertical_speed)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_SATCOMM_TELEMETRY_LEN];
    _mav_put_uint32_t(buf, 0, time);
    _mav_put_int32_t(buf, 4, latitude);
    _mav_put_int32_t(buf, 8, longitude);
    _mav_put_float(buf, 12, altitude);
    _mav_put_float(buf, 16, nav_distance);
    _mav_put_uint32_t(buf, 20, health);
    _mav_put_uint16_t(buf, 24, nav_bearing);
    _mav_put_uint16_t(buf, 26, ground_speed);
    _mav_put_uint16_t(buf, 28, air_speed);
    _mav_put_uint16_t(buf, 30, bat_voltage);
    _mav_put_uint16_t(buf, 32, bat_current);
    _mav_put_int16_t(buf, 34, roll);
    _mav_put_int16_t(buf, 36, pitch);
    _mav_put_int16_t(buf, 38, wind_speed);
    _mav_put_int16_t(buf, 40, wind_dir);
    _mav_put_int16_t(buf, 42, waypoint_num);
    _mav_put_int16_t(buf, 44, throttle);
    _mav_put_int16_t(buf, 46, vertical_speed);
    _mav_put_uint8_t(buf, 48, mode);
    _mav_put_uint8_t(buf, 49, gps_nsats);
    _mav_put_uint8_t(buf, 50, gps_hdop);
    _mav_put_uint8_t(buf, 51, gps_fixtype);
    _mav_put_uint8_t(buf, 52, bat_percent);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_SATCOMM_TELEMETRY_LEN);
#else
    mavlink_satcomm_telemetry_t packet;
    packet.time = time;
    packet.latitude = latitude;
    packet.longitude = longitude;
    packet.altitude = altitude;
    packet.nav_distance = nav_distance;
    packet.health = health;
    packet.nav_bearing = nav_bearing;
    packet.ground_speed = ground_speed;
    packet.air_speed = air_speed;
    packet.bat_voltage = bat_voltage;
    packet.bat_current = bat_current;
    packet.roll = roll;
    packet.pitch = pitch;
    packet.wind_speed = wind_speed;
    packet.wind_dir = wind_dir;
    packet.waypoint_num = waypoint_num;
    packet.throttle = throttle;
    packet.vertical_speed = vertical_speed;
    packet.mode = mode;
    packet.gps_nsats = gps_nsats;
    packet.gps_hdop = gps_hdop;
    packet.gps_fixtype = gps_fixtype;
    packet.bat_percent = bat_percent;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_SATCOMM_TELEMETRY_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_SATCOMM_TELEMETRY;
    return mavlink_finalize_message(msg, system_id, component_id, MAVLINK_MSG_ID_SATCOMM_TELEMETRY_MIN_LEN, MAVLINK_MSG_ID_SATCOMM_TELEMETRY_LEN, MAVLINK_MSG_ID_SATCOMM_TELEMETRY_CRC);
}

/**
 * @brief Pack a satcomm_telemetry message on a channel
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param time Description of the field
 * @param latitude Description of the field
 * @param longitude Description of the field
 * @param altitude Description of the field
 * @param mode Description of the field
 * @param nav_bearing Description of the field
 * @param nav_distance Description of the field
 * @param health Description of the field
 * @param ground_speed Description of the field
 * @param air_speed Description of the field
 * @param gps_nsats Description of the field
 * @param gps_hdop Description of the field
 * @param gps_fixtype Description of the field
 * @param bat_voltage Description of the field
 * @param bat_percent Description of the field
 * @param bat_current Description of the field
 * @param roll Description of the field
 * @param pitch Description of the field
 * @param wind_speed Description of the field
 * @param wind_dir Description of the field
 * @param waypoint_num Description of the field
 * @param throttle Description of the field
 * @param vertical_speed Description of the field
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_satcomm_telemetry_pack_chan(uint8_t system_id, uint8_t component_id, uint8_t chan,
                               mavlink_message_t* msg,
                                   uint32_t time,int32_t latitude,int32_t longitude,float altitude,uint8_t mode,uint16_t nav_bearing,float nav_distance,uint32_t health,uint16_t ground_speed,uint16_t air_speed,uint8_t gps_nsats,uint8_t gps_hdop,uint8_t gps_fixtype,uint16_t bat_voltage,uint8_t bat_percent,uint16_t bat_current,int16_t roll,int16_t pitch,int16_t wind_speed,int16_t wind_dir,int16_t waypoint_num,int16_t throttle,int16_t vertical_speed)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_SATCOMM_TELEMETRY_LEN];
    _mav_put_uint32_t(buf, 0, time);
    _mav_put_int32_t(buf, 4, latitude);
    _mav_put_int32_t(buf, 8, longitude);
    _mav_put_float(buf, 12, altitude);
    _mav_put_float(buf, 16, nav_distance);
    _mav_put_uint32_t(buf, 20, health);
    _mav_put_uint16_t(buf, 24, nav_bearing);
    _mav_put_uint16_t(buf, 26, ground_speed);
    _mav_put_uint16_t(buf, 28, air_speed);
    _mav_put_uint16_t(buf, 30, bat_voltage);
    _mav_put_uint16_t(buf, 32, bat_current);
    _mav_put_int16_t(buf, 34, roll);
    _mav_put_int16_t(buf, 36, pitch);
    _mav_put_int16_t(buf, 38, wind_speed);
    _mav_put_int16_t(buf, 40, wind_dir);
    _mav_put_int16_t(buf, 42, waypoint_num);
    _mav_put_int16_t(buf, 44, throttle);
    _mav_put_int16_t(buf, 46, vertical_speed);
    _mav_put_uint8_t(buf, 48, mode);
    _mav_put_uint8_t(buf, 49, gps_nsats);
    _mav_put_uint8_t(buf, 50, gps_hdop);
    _mav_put_uint8_t(buf, 51, gps_fixtype);
    _mav_put_uint8_t(buf, 52, bat_percent);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_SATCOMM_TELEMETRY_LEN);
#else
    mavlink_satcomm_telemetry_t packet;
    packet.time = time;
    packet.latitude = latitude;
    packet.longitude = longitude;
    packet.altitude = altitude;
    packet.nav_distance = nav_distance;
    packet.health = health;
    packet.nav_bearing = nav_bearing;
    packet.ground_speed = ground_speed;
    packet.air_speed = air_speed;
    packet.bat_voltage = bat_voltage;
    packet.bat_current = bat_current;
    packet.roll = roll;
    packet.pitch = pitch;
    packet.wind_speed = wind_speed;
    packet.wind_dir = wind_dir;
    packet.waypoint_num = waypoint_num;
    packet.throttle = throttle;
    packet.vertical_speed = vertical_speed;
    packet.mode = mode;
    packet.gps_nsats = gps_nsats;
    packet.gps_hdop = gps_hdop;
    packet.gps_fixtype = gps_fixtype;
    packet.bat_percent = bat_percent;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_SATCOMM_TELEMETRY_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_SATCOMM_TELEMETRY;
    return mavlink_finalize_message_chan(msg, system_id, component_id, chan, MAVLINK_MSG_ID_SATCOMM_TELEMETRY_MIN_LEN, MAVLINK_MSG_ID_SATCOMM_TELEMETRY_LEN, MAVLINK_MSG_ID_SATCOMM_TELEMETRY_CRC);
}

/**
 * @brief Encode a satcomm_telemetry struct
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 * @param satcomm_telemetry C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_satcomm_telemetry_encode(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg, const mavlink_satcomm_telemetry_t* satcomm_telemetry)
{
    return mavlink_msg_satcomm_telemetry_pack(system_id, component_id, msg, satcomm_telemetry->time, satcomm_telemetry->latitude, satcomm_telemetry->longitude, satcomm_telemetry->altitude, satcomm_telemetry->mode, satcomm_telemetry->nav_bearing, satcomm_telemetry->nav_distance, satcomm_telemetry->health, satcomm_telemetry->ground_speed, satcomm_telemetry->air_speed, satcomm_telemetry->gps_nsats, satcomm_telemetry->gps_hdop, satcomm_telemetry->gps_fixtype, satcomm_telemetry->bat_voltage, satcomm_telemetry->bat_percent, satcomm_telemetry->bat_current, satcomm_telemetry->roll, satcomm_telemetry->pitch, satcomm_telemetry->wind_speed, satcomm_telemetry->wind_dir, satcomm_telemetry->waypoint_num, satcomm_telemetry->throttle, satcomm_telemetry->vertical_speed);
}

/**
 * @brief Encode a satcomm_telemetry struct on a channel
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param satcomm_telemetry C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_satcomm_telemetry_encode_chan(uint8_t system_id, uint8_t component_id, uint8_t chan, mavlink_message_t* msg, const mavlink_satcomm_telemetry_t* satcomm_telemetry)
{
    return mavlink_msg_satcomm_telemetry_pack_chan(system_id, component_id, chan, msg, satcomm_telemetry->time, satcomm_telemetry->latitude, satcomm_telemetry->longitude, satcomm_telemetry->altitude, satcomm_telemetry->mode, satcomm_telemetry->nav_bearing, satcomm_telemetry->nav_distance, satcomm_telemetry->health, satcomm_telemetry->ground_speed, satcomm_telemetry->air_speed, satcomm_telemetry->gps_nsats, satcomm_telemetry->gps_hdop, satcomm_telemetry->gps_fixtype, satcomm_telemetry->bat_voltage, satcomm_telemetry->bat_percent, satcomm_telemetry->bat_current, satcomm_telemetry->roll, satcomm_telemetry->pitch, satcomm_telemetry->wind_speed, satcomm_telemetry->wind_dir, satcomm_telemetry->waypoint_num, satcomm_telemetry->throttle, satcomm_telemetry->vertical_speed);
}

/**
 * @brief Send a satcomm_telemetry message
 * @param chan MAVLink channel to send the message
 *
 * @param time Description of the field
 * @param latitude Description of the field
 * @param longitude Description of the field
 * @param altitude Description of the field
 * @param mode Description of the field
 * @param nav_bearing Description of the field
 * @param nav_distance Description of the field
 * @param health Description of the field
 * @param ground_speed Description of the field
 * @param air_speed Description of the field
 * @param gps_nsats Description of the field
 * @param gps_hdop Description of the field
 * @param gps_fixtype Description of the field
 * @param bat_voltage Description of the field
 * @param bat_percent Description of the field
 * @param bat_current Description of the field
 * @param roll Description of the field
 * @param pitch Description of the field
 * @param wind_speed Description of the field
 * @param wind_dir Description of the field
 * @param waypoint_num Description of the field
 * @param throttle Description of the field
 * @param vertical_speed Description of the field
 */
#ifdef MAVLINK_USE_CONVENIENCE_FUNCTIONS

static inline void mavlink_msg_satcomm_telemetry_send(mavlink_channel_t chan, uint32_t time, int32_t latitude, int32_t longitude, float altitude, uint8_t mode, uint16_t nav_bearing, float nav_distance, uint32_t health, uint16_t ground_speed, uint16_t air_speed, uint8_t gps_nsats, uint8_t gps_hdop, uint8_t gps_fixtype, uint16_t bat_voltage, uint8_t bat_percent, uint16_t bat_current, int16_t roll, int16_t pitch, int16_t wind_speed, int16_t wind_dir, int16_t waypoint_num, int16_t throttle, int16_t vertical_speed)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_SATCOMM_TELEMETRY_LEN];
    _mav_put_uint32_t(buf, 0, time);
    _mav_put_int32_t(buf, 4, latitude);
    _mav_put_int32_t(buf, 8, longitude);
    _mav_put_float(buf, 12, altitude);
    _mav_put_float(buf, 16, nav_distance);
    _mav_put_uint32_t(buf, 20, health);
    _mav_put_uint16_t(buf, 24, nav_bearing);
    _mav_put_uint16_t(buf, 26, ground_speed);
    _mav_put_uint16_t(buf, 28, air_speed);
    _mav_put_uint16_t(buf, 30, bat_voltage);
    _mav_put_uint16_t(buf, 32, bat_current);
    _mav_put_int16_t(buf, 34, roll);
    _mav_put_int16_t(buf, 36, pitch);
    _mav_put_int16_t(buf, 38, wind_speed);
    _mav_put_int16_t(buf, 40, wind_dir);
    _mav_put_int16_t(buf, 42, waypoint_num);
    _mav_put_int16_t(buf, 44, throttle);
    _mav_put_int16_t(buf, 46, vertical_speed);
    _mav_put_uint8_t(buf, 48, mode);
    _mav_put_uint8_t(buf, 49, gps_nsats);
    _mav_put_uint8_t(buf, 50, gps_hdop);
    _mav_put_uint8_t(buf, 51, gps_fixtype);
    _mav_put_uint8_t(buf, 52, bat_percent);

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_SATCOMM_TELEMETRY, buf, MAVLINK_MSG_ID_SATCOMM_TELEMETRY_MIN_LEN, MAVLINK_MSG_ID_SATCOMM_TELEMETRY_LEN, MAVLINK_MSG_ID_SATCOMM_TELEMETRY_CRC);
#else
    mavlink_satcomm_telemetry_t packet;
    packet.time = time;
    packet.latitude = latitude;
    packet.longitude = longitude;
    packet.altitude = altitude;
    packet.nav_distance = nav_distance;
    packet.health = health;
    packet.nav_bearing = nav_bearing;
    packet.ground_speed = ground_speed;
    packet.air_speed = air_speed;
    packet.bat_voltage = bat_voltage;
    packet.bat_current = bat_current;
    packet.roll = roll;
    packet.pitch = pitch;
    packet.wind_speed = wind_speed;
    packet.wind_dir = wind_dir;
    packet.waypoint_num = waypoint_num;
    packet.throttle = throttle;
    packet.vertical_speed = vertical_speed;
    packet.mode = mode;
    packet.gps_nsats = gps_nsats;
    packet.gps_hdop = gps_hdop;
    packet.gps_fixtype = gps_fixtype;
    packet.bat_percent = bat_percent;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_SATCOMM_TELEMETRY, (const char *)&packet, MAVLINK_MSG_ID_SATCOMM_TELEMETRY_MIN_LEN, MAVLINK_MSG_ID_SATCOMM_TELEMETRY_LEN, MAVLINK_MSG_ID_SATCOMM_TELEMETRY_CRC);
#endif
}

/**
 * @brief Send a satcomm_telemetry message
 * @param chan MAVLink channel to send the message
 * @param struct The MAVLink struct to serialize
 */
static inline void mavlink_msg_satcomm_telemetry_send_struct(mavlink_channel_t chan, const mavlink_satcomm_telemetry_t* satcomm_telemetry)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    mavlink_msg_satcomm_telemetry_send(chan, satcomm_telemetry->time, satcomm_telemetry->latitude, satcomm_telemetry->longitude, satcomm_telemetry->altitude, satcomm_telemetry->mode, satcomm_telemetry->nav_bearing, satcomm_telemetry->nav_distance, satcomm_telemetry->health, satcomm_telemetry->ground_speed, satcomm_telemetry->air_speed, satcomm_telemetry->gps_nsats, satcomm_telemetry->gps_hdop, satcomm_telemetry->gps_fixtype, satcomm_telemetry->bat_voltage, satcomm_telemetry->bat_percent, satcomm_telemetry->bat_current, satcomm_telemetry->roll, satcomm_telemetry->pitch, satcomm_telemetry->wind_speed, satcomm_telemetry->wind_dir, satcomm_telemetry->waypoint_num, satcomm_telemetry->throttle, satcomm_telemetry->vertical_speed);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_SATCOMM_TELEMETRY, (const char *)satcomm_telemetry, MAVLINK_MSG_ID_SATCOMM_TELEMETRY_MIN_LEN, MAVLINK_MSG_ID_SATCOMM_TELEMETRY_LEN, MAVLINK_MSG_ID_SATCOMM_TELEMETRY_CRC);
#endif
}

#if MAVLINK_MSG_ID_SATCOMM_TELEMETRY_LEN <= MAVLINK_MAX_PAYLOAD_LEN
/*
  This varient of _send() can be used to save stack space by re-using
  memory from the receive buffer.  The caller provides a
  mavlink_message_t which is the size of a full mavlink message. This
  is usually the receive buffer for the channel, and allows a reply to an
  incoming message with minimum stack space usage.
 */
static inline void mavlink_msg_satcomm_telemetry_send_buf(mavlink_message_t *msgbuf, mavlink_channel_t chan,  uint32_t time, int32_t latitude, int32_t longitude, float altitude, uint8_t mode, uint16_t nav_bearing, float nav_distance, uint32_t health, uint16_t ground_speed, uint16_t air_speed, uint8_t gps_nsats, uint8_t gps_hdop, uint8_t gps_fixtype, uint16_t bat_voltage, uint8_t bat_percent, uint16_t bat_current, int16_t roll, int16_t pitch, int16_t wind_speed, int16_t wind_dir, int16_t waypoint_num, int16_t throttle, int16_t vertical_speed)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char *buf = (char *)msgbuf;
    _mav_put_uint32_t(buf, 0, time);
    _mav_put_int32_t(buf, 4, latitude);
    _mav_put_int32_t(buf, 8, longitude);
    _mav_put_float(buf, 12, altitude);
    _mav_put_float(buf, 16, nav_distance);
    _mav_put_uint32_t(buf, 20, health);
    _mav_put_uint16_t(buf, 24, nav_bearing);
    _mav_put_uint16_t(buf, 26, ground_speed);
    _mav_put_uint16_t(buf, 28, air_speed);
    _mav_put_uint16_t(buf, 30, bat_voltage);
    _mav_put_uint16_t(buf, 32, bat_current);
    _mav_put_int16_t(buf, 34, roll);
    _mav_put_int16_t(buf, 36, pitch);
    _mav_put_int16_t(buf, 38, wind_speed);
    _mav_put_int16_t(buf, 40, wind_dir);
    _mav_put_int16_t(buf, 42, waypoint_num);
    _mav_put_int16_t(buf, 44, throttle);
    _mav_put_int16_t(buf, 46, vertical_speed);
    _mav_put_uint8_t(buf, 48, mode);
    _mav_put_uint8_t(buf, 49, gps_nsats);
    _mav_put_uint8_t(buf, 50, gps_hdop);
    _mav_put_uint8_t(buf, 51, gps_fixtype);
    _mav_put_uint8_t(buf, 52, bat_percent);

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_SATCOMM_TELEMETRY, buf, MAVLINK_MSG_ID_SATCOMM_TELEMETRY_MIN_LEN, MAVLINK_MSG_ID_SATCOMM_TELEMETRY_LEN, MAVLINK_MSG_ID_SATCOMM_TELEMETRY_CRC);
#else
    mavlink_satcomm_telemetry_t *packet = (mavlink_satcomm_telemetry_t *)msgbuf;
    packet->time = time;
    packet->latitude = latitude;
    packet->longitude = longitude;
    packet->altitude = altitude;
    packet->nav_distance = nav_distance;
    packet->health = health;
    packet->nav_bearing = nav_bearing;
    packet->ground_speed = ground_speed;
    packet->air_speed = air_speed;
    packet->bat_voltage = bat_voltage;
    packet->bat_current = bat_current;
    packet->roll = roll;
    packet->pitch = pitch;
    packet->wind_speed = wind_speed;
    packet->wind_dir = wind_dir;
    packet->waypoint_num = waypoint_num;
    packet->throttle = throttle;
    packet->vertical_speed = vertical_speed;
    packet->mode = mode;
    packet->gps_nsats = gps_nsats;
    packet->gps_hdop = gps_hdop;
    packet->gps_fixtype = gps_fixtype;
    packet->bat_percent = bat_percent;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_SATCOMM_TELEMETRY, (const char *)packet, MAVLINK_MSG_ID_SATCOMM_TELEMETRY_MIN_LEN, MAVLINK_MSG_ID_SATCOMM_TELEMETRY_LEN, MAVLINK_MSG_ID_SATCOMM_TELEMETRY_CRC);
#endif
}
#endif

#endif

// MESSAGE SATCOMM_TELEMETRY UNPACKING


/**
 * @brief Get field time from satcomm_telemetry message
 *
 * @return Description of the field
 */
static inline uint32_t mavlink_msg_satcomm_telemetry_get_time(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint32_t(msg,  0);
}

/**
 * @brief Get field latitude from satcomm_telemetry message
 *
 * @return Description of the field
 */
static inline int32_t mavlink_msg_satcomm_telemetry_get_latitude(const mavlink_message_t* msg)
{
    return _MAV_RETURN_int32_t(msg,  4);
}

/**
 * @brief Get field longitude from satcomm_telemetry message
 *
 * @return Description of the field
 */
static inline int32_t mavlink_msg_satcomm_telemetry_get_longitude(const mavlink_message_t* msg)
{
    return _MAV_RETURN_int32_t(msg,  8);
}

/**
 * @brief Get field altitude from satcomm_telemetry message
 *
 * @return Description of the field
 */
static inline float mavlink_msg_satcomm_telemetry_get_altitude(const mavlink_message_t* msg)
{
    return _MAV_RETURN_float(msg,  12);
}

/**
 * @brief Get field mode from satcomm_telemetry message
 *
 * @return Description of the field
 */
static inline uint8_t mavlink_msg_satcomm_telemetry_get_mode(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint8_t(msg,  48);
}

/**
 * @brief Get field nav_bearing from satcomm_telemetry message
 *
 * @return Description of the field
 */
static inline uint16_t mavlink_msg_satcomm_telemetry_get_nav_bearing(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint16_t(msg,  24);
}

/**
 * @brief Get field nav_distance from satcomm_telemetry message
 *
 * @return Description of the field
 */
static inline float mavlink_msg_satcomm_telemetry_get_nav_distance(const mavlink_message_t* msg)
{
    return _MAV_RETURN_float(msg,  16);
}

/**
 * @brief Get field health from satcomm_telemetry message
 *
 * @return Description of the field
 */
static inline uint32_t mavlink_msg_satcomm_telemetry_get_health(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint32_t(msg,  20);
}

/**
 * @brief Get field ground_speed from satcomm_telemetry message
 *
 * @return Description of the field
 */
static inline uint16_t mavlink_msg_satcomm_telemetry_get_ground_speed(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint16_t(msg,  26);
}

/**
 * @brief Get field air_speed from satcomm_telemetry message
 *
 * @return Description of the field
 */
static inline uint16_t mavlink_msg_satcomm_telemetry_get_air_speed(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint16_t(msg,  28);
}

/**
 * @brief Get field gps_nsats from satcomm_telemetry message
 *
 * @return Description of the field
 */
static inline uint8_t mavlink_msg_satcomm_telemetry_get_gps_nsats(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint8_t(msg,  49);
}

/**
 * @brief Get field gps_hdop from satcomm_telemetry message
 *
 * @return Description of the field
 */
static inline uint8_t mavlink_msg_satcomm_telemetry_get_gps_hdop(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint8_t(msg,  50);
}

/**
 * @brief Get field gps_fixtype from satcomm_telemetry message
 *
 * @return Description of the field
 */
static inline uint8_t mavlink_msg_satcomm_telemetry_get_gps_fixtype(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint8_t(msg,  51);
}

/**
 * @brief Get field bat_voltage from satcomm_telemetry message
 *
 * @return Description of the field
 */
static inline uint16_t mavlink_msg_satcomm_telemetry_get_bat_voltage(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint16_t(msg,  30);
}

/**
 * @brief Get field bat_percent from satcomm_telemetry message
 *
 * @return Description of the field
 */
static inline uint8_t mavlink_msg_satcomm_telemetry_get_bat_percent(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint8_t(msg,  52);
}

/**
 * @brief Get field bat_current from satcomm_telemetry message
 *
 * @return Description of the field
 */
static inline uint16_t mavlink_msg_satcomm_telemetry_get_bat_current(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint16_t(msg,  32);
}

/**
 * @brief Get field roll from satcomm_telemetry message
 *
 * @return Description of the field
 */
static inline int16_t mavlink_msg_satcomm_telemetry_get_roll(const mavlink_message_t* msg)
{
    return _MAV_RETURN_int16_t(msg,  34);
}

/**
 * @brief Get field pitch from satcomm_telemetry message
 *
 * @return Description of the field
 */
static inline int16_t mavlink_msg_satcomm_telemetry_get_pitch(const mavlink_message_t* msg)
{
    return _MAV_RETURN_int16_t(msg,  36);
}

/**
 * @brief Get field wind_speed from satcomm_telemetry message
 *
 * @return Description of the field
 */
static inline int16_t mavlink_msg_satcomm_telemetry_get_wind_speed(const mavlink_message_t* msg)
{
    return _MAV_RETURN_int16_t(msg,  38);
}

/**
 * @brief Get field wind_dir from satcomm_telemetry message
 *
 * @return Description of the field
 */
static inline int16_t mavlink_msg_satcomm_telemetry_get_wind_dir(const mavlink_message_t* msg)
{
    return _MAV_RETURN_int16_t(msg,  40);
}

/**
 * @brief Get field waypoint_num from satcomm_telemetry message
 *
 * @return Description of the field
 */
static inline int16_t mavlink_msg_satcomm_telemetry_get_waypoint_num(const mavlink_message_t* msg)
{
    return _MAV_RETURN_int16_t(msg,  42);
}

/**
 * @brief Get field throttle from satcomm_telemetry message
 *
 * @return Description of the field
 */
static inline int16_t mavlink_msg_satcomm_telemetry_get_throttle(const mavlink_message_t* msg)
{
    return _MAV_RETURN_int16_t(msg,  44);
}

/**
 * @brief Get field vertical_speed from satcomm_telemetry message
 *
 * @return Description of the field
 */
static inline int16_t mavlink_msg_satcomm_telemetry_get_vertical_speed(const mavlink_message_t* msg)
{
    return _MAV_RETURN_int16_t(msg,  46);
}

/**
 * @brief Decode a satcomm_telemetry message into a struct
 *
 * @param msg The message to decode
 * @param satcomm_telemetry C-struct to decode the message contents into
 */
static inline void mavlink_msg_satcomm_telemetry_decode(const mavlink_message_t* msg, mavlink_satcomm_telemetry_t* satcomm_telemetry)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    satcomm_telemetry->time = mavlink_msg_satcomm_telemetry_get_time(msg);
    satcomm_telemetry->latitude = mavlink_msg_satcomm_telemetry_get_latitude(msg);
    satcomm_telemetry->longitude = mavlink_msg_satcomm_telemetry_get_longitude(msg);
    satcomm_telemetry->altitude = mavlink_msg_satcomm_telemetry_get_altitude(msg);
    satcomm_telemetry->nav_distance = mavlink_msg_satcomm_telemetry_get_nav_distance(msg);
    satcomm_telemetry->health = mavlink_msg_satcomm_telemetry_get_health(msg);
    satcomm_telemetry->nav_bearing = mavlink_msg_satcomm_telemetry_get_nav_bearing(msg);
    satcomm_telemetry->ground_speed = mavlink_msg_satcomm_telemetry_get_ground_speed(msg);
    satcomm_telemetry->air_speed = mavlink_msg_satcomm_telemetry_get_air_speed(msg);
    satcomm_telemetry->bat_voltage = mavlink_msg_satcomm_telemetry_get_bat_voltage(msg);
    satcomm_telemetry->bat_current = mavlink_msg_satcomm_telemetry_get_bat_current(msg);
    satcomm_telemetry->roll = mavlink_msg_satcomm_telemetry_get_roll(msg);
    satcomm_telemetry->pitch = mavlink_msg_satcomm_telemetry_get_pitch(msg);
    satcomm_telemetry->wind_speed = mavlink_msg_satcomm_telemetry_get_wind_speed(msg);
    satcomm_telemetry->wind_dir = mavlink_msg_satcomm_telemetry_get_wind_dir(msg);
    satcomm_telemetry->waypoint_num = mavlink_msg_satcomm_telemetry_get_waypoint_num(msg);
    satcomm_telemetry->throttle = mavlink_msg_satcomm_telemetry_get_throttle(msg);
    satcomm_telemetry->vertical_speed = mavlink_msg_satcomm_telemetry_get_vertical_speed(msg);
    satcomm_telemetry->mode = mavlink_msg_satcomm_telemetry_get_mode(msg);
    satcomm_telemetry->gps_nsats = mavlink_msg_satcomm_telemetry_get_gps_nsats(msg);
    satcomm_telemetry->gps_hdop = mavlink_msg_satcomm_telemetry_get_gps_hdop(msg);
    satcomm_telemetry->gps_fixtype = mavlink_msg_satcomm_telemetry_get_gps_fixtype(msg);
    satcomm_telemetry->bat_percent = mavlink_msg_satcomm_telemetry_get_bat_percent(msg);
#else
        uint8_t len = msg->len < MAVLINK_MSG_ID_SATCOMM_TELEMETRY_LEN? msg->len : MAVLINK_MSG_ID_SATCOMM_TELEMETRY_LEN;
        memset(satcomm_telemetry, 0, MAVLINK_MSG_ID_SATCOMM_TELEMETRY_LEN);
    memcpy(satcomm_telemetry, _MAV_PAYLOAD(msg), len);
#endif
}
