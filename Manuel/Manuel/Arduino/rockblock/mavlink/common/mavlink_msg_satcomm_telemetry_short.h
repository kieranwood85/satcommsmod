#pragma once
// MESSAGE SATCOMM_TELEMETRY_SHORT PACKING

#define MAVLINK_MSG_ID_SATCOMM_TELEMETRY_SHORT 181

MAVPACKED(
typedef struct __mavlink_satcomm_telemetry_short_t {
 uint32_t time; /*< Description of the field*/
 int32_t latitude; /*< Description of the field*/
 int32_t longitude; /*< Description of the field*/
 float altitude; /*< Description of the field*/
 uint16_t nav_bearing; /*< Description of the field*/
 uint16_t ground_speed; /*< Description of the field*/
 uint16_t air_speed; /*< Description of the field*/
 int16_t roll; /*< Description of the field*/
 int16_t pitch; /*< Description of the field*/
 int16_t waypoint_num; /*< Description of the field*/
 uint8_t mode; /*< Description of the field*/
 uint8_t bat_percent; /*< Description of the field*/
}) mavlink_satcomm_telemetry_short_t;

#define MAVLINK_MSG_ID_SATCOMM_TELEMETRY_SHORT_LEN 30
#define MAVLINK_MSG_ID_SATCOMM_TELEMETRY_SHORT_MIN_LEN 30
#define MAVLINK_MSG_ID_181_LEN 30
#define MAVLINK_MSG_ID_181_MIN_LEN 30

#define MAVLINK_MSG_ID_SATCOMM_TELEMETRY_SHORT_CRC 2
#define MAVLINK_MSG_ID_181_CRC 2



#if MAVLINK_COMMAND_24BIT
#define MAVLINK_MESSAGE_INFO_SATCOMM_TELEMETRY_SHORT { \
    181, \
    "SATCOMM_TELEMETRY_SHORT", \
    12, \
    {  { "time", NULL, MAVLINK_TYPE_UINT32_T, 0, 0, offsetof(mavlink_satcomm_telemetry_short_t, time) }, \
         { "latitude", NULL, MAVLINK_TYPE_INT32_T, 0, 4, offsetof(mavlink_satcomm_telemetry_short_t, latitude) }, \
         { "longitude", NULL, MAVLINK_TYPE_INT32_T, 0, 8, offsetof(mavlink_satcomm_telemetry_short_t, longitude) }, \
         { "altitude", NULL, MAVLINK_TYPE_FLOAT, 0, 12, offsetof(mavlink_satcomm_telemetry_short_t, altitude) }, \
         { "nav_bearing", NULL, MAVLINK_TYPE_UINT16_T, 0, 16, offsetof(mavlink_satcomm_telemetry_short_t, nav_bearing) }, \
         { "ground_speed", NULL, MAVLINK_TYPE_UINT16_T, 0, 18, offsetof(mavlink_satcomm_telemetry_short_t, ground_speed) }, \
         { "air_speed", NULL, MAVLINK_TYPE_UINT16_T, 0, 20, offsetof(mavlink_satcomm_telemetry_short_t, air_speed) }, \
         { "roll", NULL, MAVLINK_TYPE_INT16_T, 0, 22, offsetof(mavlink_satcomm_telemetry_short_t, roll) }, \
         { "pitch", NULL, MAVLINK_TYPE_INT16_T, 0, 24, offsetof(mavlink_satcomm_telemetry_short_t, pitch) }, \
         { "waypoint_num", NULL, MAVLINK_TYPE_INT16_T, 0, 26, offsetof(mavlink_satcomm_telemetry_short_t, waypoint_num) }, \
         { "mode", NULL, MAVLINK_TYPE_UINT8_T, 0, 28, offsetof(mavlink_satcomm_telemetry_short_t, mode) }, \
         { "bat_percent", NULL, MAVLINK_TYPE_UINT8_T, 0, 29, offsetof(mavlink_satcomm_telemetry_short_t, bat_percent) }, \
         } \
}
#else
#define MAVLINK_MESSAGE_INFO_SATCOMM_TELEMETRY_SHORT { \
    "SATCOMM_TELEMETRY_SHORT", \
    12, \
    {  { "time", NULL, MAVLINK_TYPE_UINT32_T, 0, 0, offsetof(mavlink_satcomm_telemetry_short_t, time) }, \
         { "latitude", NULL, MAVLINK_TYPE_INT32_T, 0, 4, offsetof(mavlink_satcomm_telemetry_short_t, latitude) }, \
         { "longitude", NULL, MAVLINK_TYPE_INT32_T, 0, 8, offsetof(mavlink_satcomm_telemetry_short_t, longitude) }, \
         { "altitude", NULL, MAVLINK_TYPE_FLOAT, 0, 12, offsetof(mavlink_satcomm_telemetry_short_t, altitude) }, \
         { "nav_bearing", NULL, MAVLINK_TYPE_UINT16_T, 0, 16, offsetof(mavlink_satcomm_telemetry_short_t, nav_bearing) }, \
         { "ground_speed", NULL, MAVLINK_TYPE_UINT16_T, 0, 18, offsetof(mavlink_satcomm_telemetry_short_t, ground_speed) }, \
         { "air_speed", NULL, MAVLINK_TYPE_UINT16_T, 0, 20, offsetof(mavlink_satcomm_telemetry_short_t, air_speed) }, \
         { "roll", NULL, MAVLINK_TYPE_INT16_T, 0, 22, offsetof(mavlink_satcomm_telemetry_short_t, roll) }, \
         { "pitch", NULL, MAVLINK_TYPE_INT16_T, 0, 24, offsetof(mavlink_satcomm_telemetry_short_t, pitch) }, \
         { "waypoint_num", NULL, MAVLINK_TYPE_INT16_T, 0, 26, offsetof(mavlink_satcomm_telemetry_short_t, waypoint_num) }, \
         { "mode", NULL, MAVLINK_TYPE_UINT8_T, 0, 28, offsetof(mavlink_satcomm_telemetry_short_t, mode) }, \
         { "bat_percent", NULL, MAVLINK_TYPE_UINT8_T, 0, 29, offsetof(mavlink_satcomm_telemetry_short_t, bat_percent) }, \
         } \
}
#endif

/**
 * @brief Pack a satcomm_telemetry_short message
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 *
 * @param time Description of the field
 * @param latitude Description of the field
 * @param longitude Description of the field
 * @param altitude Description of the field
 * @param mode Description of the field
 * @param nav_bearing Description of the field
 * @param ground_speed Description of the field
 * @param air_speed Description of the field
 * @param bat_percent Description of the field
 * @param roll Description of the field
 * @param pitch Description of the field
 * @param waypoint_num Description of the field
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_satcomm_telemetry_short_pack(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg,
                               uint32_t time, int32_t latitude, int32_t longitude, float altitude, uint8_t mode, uint16_t nav_bearing, uint16_t ground_speed, uint16_t air_speed, uint8_t bat_percent, int16_t roll, int16_t pitch, int16_t waypoint_num)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_SATCOMM_TELEMETRY_SHORT_LEN];
    _mav_put_uint32_t(buf, 0, time);
    _mav_put_int32_t(buf, 4, latitude);
    _mav_put_int32_t(buf, 8, longitude);
    _mav_put_float(buf, 12, altitude);
    _mav_put_uint16_t(buf, 16, nav_bearing);
    _mav_put_uint16_t(buf, 18, ground_speed);
    _mav_put_uint16_t(buf, 20, air_speed);
    _mav_put_int16_t(buf, 22, roll);
    _mav_put_int16_t(buf, 24, pitch);
    _mav_put_int16_t(buf, 26, waypoint_num);
    _mav_put_uint8_t(buf, 28, mode);
    _mav_put_uint8_t(buf, 29, bat_percent);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_SATCOMM_TELEMETRY_SHORT_LEN);
#else
    mavlink_satcomm_telemetry_short_t packet;
    packet.time = time;
    packet.latitude = latitude;
    packet.longitude = longitude;
    packet.altitude = altitude;
    packet.nav_bearing = nav_bearing;
    packet.ground_speed = ground_speed;
    packet.air_speed = air_speed;
    packet.roll = roll;
    packet.pitch = pitch;
    packet.waypoint_num = waypoint_num;
    packet.mode = mode;
    packet.bat_percent = bat_percent;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_SATCOMM_TELEMETRY_SHORT_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_SATCOMM_TELEMETRY_SHORT;
    return mavlink_finalize_message(msg, system_id, component_id, MAVLINK_MSG_ID_SATCOMM_TELEMETRY_SHORT_MIN_LEN, MAVLINK_MSG_ID_SATCOMM_TELEMETRY_SHORT_LEN, MAVLINK_MSG_ID_SATCOMM_TELEMETRY_SHORT_CRC);
}

/**
 * @brief Pack a satcomm_telemetry_short message on a channel
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param time Description of the field
 * @param latitude Description of the field
 * @param longitude Description of the field
 * @param altitude Description of the field
 * @param mode Description of the field
 * @param nav_bearing Description of the field
 * @param ground_speed Description of the field
 * @param air_speed Description of the field
 * @param bat_percent Description of the field
 * @param roll Description of the field
 * @param pitch Description of the field
 * @param waypoint_num Description of the field
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_satcomm_telemetry_short_pack_chan(uint8_t system_id, uint8_t component_id, uint8_t chan,
                               mavlink_message_t* msg,
                                   uint32_t time,int32_t latitude,int32_t longitude,float altitude,uint8_t mode,uint16_t nav_bearing,uint16_t ground_speed,uint16_t air_speed,uint8_t bat_percent,int16_t roll,int16_t pitch,int16_t waypoint_num)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_SATCOMM_TELEMETRY_SHORT_LEN];
    _mav_put_uint32_t(buf, 0, time);
    _mav_put_int32_t(buf, 4, latitude);
    _mav_put_int32_t(buf, 8, longitude);
    _mav_put_float(buf, 12, altitude);
    _mav_put_uint16_t(buf, 16, nav_bearing);
    _mav_put_uint16_t(buf, 18, ground_speed);
    _mav_put_uint16_t(buf, 20, air_speed);
    _mav_put_int16_t(buf, 22, roll);
    _mav_put_int16_t(buf, 24, pitch);
    _mav_put_int16_t(buf, 26, waypoint_num);
    _mav_put_uint8_t(buf, 28, mode);
    _mav_put_uint8_t(buf, 29, bat_percent);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_SATCOMM_TELEMETRY_SHORT_LEN);
#else
    mavlink_satcomm_telemetry_short_t packet;
    packet.time = time;
    packet.latitude = latitude;
    packet.longitude = longitude;
    packet.altitude = altitude;
    packet.nav_bearing = nav_bearing;
    packet.ground_speed = ground_speed;
    packet.air_speed = air_speed;
    packet.roll = roll;
    packet.pitch = pitch;
    packet.waypoint_num = waypoint_num;
    packet.mode = mode;
    packet.bat_percent = bat_percent;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_SATCOMM_TELEMETRY_SHORT_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_SATCOMM_TELEMETRY_SHORT;
    return mavlink_finalize_message_chan(msg, system_id, component_id, chan, MAVLINK_MSG_ID_SATCOMM_TELEMETRY_SHORT_MIN_LEN, MAVLINK_MSG_ID_SATCOMM_TELEMETRY_SHORT_LEN, MAVLINK_MSG_ID_SATCOMM_TELEMETRY_SHORT_CRC);
}

/**
 * @brief Encode a satcomm_telemetry_short struct
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 * @param satcomm_telemetry_short C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_satcomm_telemetry_short_encode(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg, const mavlink_satcomm_telemetry_short_t* satcomm_telemetry_short)
{
    return mavlink_msg_satcomm_telemetry_short_pack(system_id, component_id, msg, satcomm_telemetry_short->time, satcomm_telemetry_short->latitude, satcomm_telemetry_short->longitude, satcomm_telemetry_short->altitude, satcomm_telemetry_short->mode, satcomm_telemetry_short->nav_bearing, satcomm_telemetry_short->ground_speed, satcomm_telemetry_short->air_speed, satcomm_telemetry_short->bat_percent, satcomm_telemetry_short->roll, satcomm_telemetry_short->pitch, satcomm_telemetry_short->waypoint_num);
}

/**
 * @brief Encode a satcomm_telemetry_short struct on a channel
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param satcomm_telemetry_short C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_satcomm_telemetry_short_encode_chan(uint8_t system_id, uint8_t component_id, uint8_t chan, mavlink_message_t* msg, const mavlink_satcomm_telemetry_short_t* satcomm_telemetry_short)
{
    return mavlink_msg_satcomm_telemetry_short_pack_chan(system_id, component_id, chan, msg, satcomm_telemetry_short->time, satcomm_telemetry_short->latitude, satcomm_telemetry_short->longitude, satcomm_telemetry_short->altitude, satcomm_telemetry_short->mode, satcomm_telemetry_short->nav_bearing, satcomm_telemetry_short->ground_speed, satcomm_telemetry_short->air_speed, satcomm_telemetry_short->bat_percent, satcomm_telemetry_short->roll, satcomm_telemetry_short->pitch, satcomm_telemetry_short->waypoint_num);
}

/**
 * @brief Send a satcomm_telemetry_short message
 * @param chan MAVLink channel to send the message
 *
 * @param time Description of the field
 * @param latitude Description of the field
 * @param longitude Description of the field
 * @param altitude Description of the field
 * @param mode Description of the field
 * @param nav_bearing Description of the field
 * @param ground_speed Description of the field
 * @param air_speed Description of the field
 * @param bat_percent Description of the field
 * @param roll Description of the field
 * @param pitch Description of the field
 * @param waypoint_num Description of the field
 */
#ifdef MAVLINK_USE_CONVENIENCE_FUNCTIONS

static inline void mavlink_msg_satcomm_telemetry_short_send(mavlink_channel_t chan, uint32_t time, int32_t latitude, int32_t longitude, float altitude, uint8_t mode, uint16_t nav_bearing, uint16_t ground_speed, uint16_t air_speed, uint8_t bat_percent, int16_t roll, int16_t pitch, int16_t waypoint_num)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_SATCOMM_TELEMETRY_SHORT_LEN];
    _mav_put_uint32_t(buf, 0, time);
    _mav_put_int32_t(buf, 4, latitude);
    _mav_put_int32_t(buf, 8, longitude);
    _mav_put_float(buf, 12, altitude);
    _mav_put_uint16_t(buf, 16, nav_bearing);
    _mav_put_uint16_t(buf, 18, ground_speed);
    _mav_put_uint16_t(buf, 20, air_speed);
    _mav_put_int16_t(buf, 22, roll);
    _mav_put_int16_t(buf, 24, pitch);
    _mav_put_int16_t(buf, 26, waypoint_num);
    _mav_put_uint8_t(buf, 28, mode);
    _mav_put_uint8_t(buf, 29, bat_percent);

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_SATCOMM_TELEMETRY_SHORT, buf, MAVLINK_MSG_ID_SATCOMM_TELEMETRY_SHORT_MIN_LEN, MAVLINK_MSG_ID_SATCOMM_TELEMETRY_SHORT_LEN, MAVLINK_MSG_ID_SATCOMM_TELEMETRY_SHORT_CRC);
#else
    mavlink_satcomm_telemetry_short_t packet;
    packet.time = time;
    packet.latitude = latitude;
    packet.longitude = longitude;
    packet.altitude = altitude;
    packet.nav_bearing = nav_bearing;
    packet.ground_speed = ground_speed;
    packet.air_speed = air_speed;
    packet.roll = roll;
    packet.pitch = pitch;
    packet.waypoint_num = waypoint_num;
    packet.mode = mode;
    packet.bat_percent = bat_percent;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_SATCOMM_TELEMETRY_SHORT, (const char *)&packet, MAVLINK_MSG_ID_SATCOMM_TELEMETRY_SHORT_MIN_LEN, MAVLINK_MSG_ID_SATCOMM_TELEMETRY_SHORT_LEN, MAVLINK_MSG_ID_SATCOMM_TELEMETRY_SHORT_CRC);
#endif
}

/**
 * @brief Send a satcomm_telemetry_short message
 * @param chan MAVLink channel to send the message
 * @param struct The MAVLink struct to serialize
 */
static inline void mavlink_msg_satcomm_telemetry_short_send_struct(mavlink_channel_t chan, const mavlink_satcomm_telemetry_short_t* satcomm_telemetry_short)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    mavlink_msg_satcomm_telemetry_short_send(chan, satcomm_telemetry_short->time, satcomm_telemetry_short->latitude, satcomm_telemetry_short->longitude, satcomm_telemetry_short->altitude, satcomm_telemetry_short->mode, satcomm_telemetry_short->nav_bearing, satcomm_telemetry_short->ground_speed, satcomm_telemetry_short->air_speed, satcomm_telemetry_short->bat_percent, satcomm_telemetry_short->roll, satcomm_telemetry_short->pitch, satcomm_telemetry_short->waypoint_num);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_SATCOMM_TELEMETRY_SHORT, (const char *)satcomm_telemetry_short, MAVLINK_MSG_ID_SATCOMM_TELEMETRY_SHORT_MIN_LEN, MAVLINK_MSG_ID_SATCOMM_TELEMETRY_SHORT_LEN, MAVLINK_MSG_ID_SATCOMM_TELEMETRY_SHORT_CRC);
#endif
}

#if MAVLINK_MSG_ID_SATCOMM_TELEMETRY_SHORT_LEN <= MAVLINK_MAX_PAYLOAD_LEN
/*
  This varient of _send() can be used to save stack space by re-using
  memory from the receive buffer.  The caller provides a
  mavlink_message_t which is the size of a full mavlink message. This
  is usually the receive buffer for the channel, and allows a reply to an
  incoming message with minimum stack space usage.
 */
static inline void mavlink_msg_satcomm_telemetry_short_send_buf(mavlink_message_t *msgbuf, mavlink_channel_t chan,  uint32_t time, int32_t latitude, int32_t longitude, float altitude, uint8_t mode, uint16_t nav_bearing, uint16_t ground_speed, uint16_t air_speed, uint8_t bat_percent, int16_t roll, int16_t pitch, int16_t waypoint_num)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char *buf = (char *)msgbuf;
    _mav_put_uint32_t(buf, 0, time);
    _mav_put_int32_t(buf, 4, latitude);
    _mav_put_int32_t(buf, 8, longitude);
    _mav_put_float(buf, 12, altitude);
    _mav_put_uint16_t(buf, 16, nav_bearing);
    _mav_put_uint16_t(buf, 18, ground_speed);
    _mav_put_uint16_t(buf, 20, air_speed);
    _mav_put_int16_t(buf, 22, roll);
    _mav_put_int16_t(buf, 24, pitch);
    _mav_put_int16_t(buf, 26, waypoint_num);
    _mav_put_uint8_t(buf, 28, mode);
    _mav_put_uint8_t(buf, 29, bat_percent);

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_SATCOMM_TELEMETRY_SHORT, buf, MAVLINK_MSG_ID_SATCOMM_TELEMETRY_SHORT_MIN_LEN, MAVLINK_MSG_ID_SATCOMM_TELEMETRY_SHORT_LEN, MAVLINK_MSG_ID_SATCOMM_TELEMETRY_SHORT_CRC);
#else
    mavlink_satcomm_telemetry_short_t *packet = (mavlink_satcomm_telemetry_short_t *)msgbuf;
    packet->time = time;
    packet->latitude = latitude;
    packet->longitude = longitude;
    packet->altitude = altitude;
    packet->nav_bearing = nav_bearing;
    packet->ground_speed = ground_speed;
    packet->air_speed = air_speed;
    packet->roll = roll;
    packet->pitch = pitch;
    packet->waypoint_num = waypoint_num;
    packet->mode = mode;
    packet->bat_percent = bat_percent;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_SATCOMM_TELEMETRY_SHORT, (const char *)packet, MAVLINK_MSG_ID_SATCOMM_TELEMETRY_SHORT_MIN_LEN, MAVLINK_MSG_ID_SATCOMM_TELEMETRY_SHORT_LEN, MAVLINK_MSG_ID_SATCOMM_TELEMETRY_SHORT_CRC);
#endif
}
#endif

#endif

// MESSAGE SATCOMM_TELEMETRY_SHORT UNPACKING


/**
 * @brief Get field time from satcomm_telemetry_short message
 *
 * @return Description of the field
 */
static inline uint32_t mavlink_msg_satcomm_telemetry_short_get_time(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint32_t(msg,  0);
}

/**
 * @brief Get field latitude from satcomm_telemetry_short message
 *
 * @return Description of the field
 */
static inline int32_t mavlink_msg_satcomm_telemetry_short_get_latitude(const mavlink_message_t* msg)
{
    return _MAV_RETURN_int32_t(msg,  4);
}

/**
 * @brief Get field longitude from satcomm_telemetry_short message
 *
 * @return Description of the field
 */
static inline int32_t mavlink_msg_satcomm_telemetry_short_get_longitude(const mavlink_message_t* msg)
{
    return _MAV_RETURN_int32_t(msg,  8);
}

/**
 * @brief Get field altitude from satcomm_telemetry_short message
 *
 * @return Description of the field
 */
static inline float mavlink_msg_satcomm_telemetry_short_get_altitude(const mavlink_message_t* msg)
{
    return _MAV_RETURN_float(msg,  12);
}

/**
 * @brief Get field mode from satcomm_telemetry_short message
 *
 * @return Description of the field
 */
static inline uint8_t mavlink_msg_satcomm_telemetry_short_get_mode(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint8_t(msg,  28);
}

/**
 * @brief Get field nav_bearing from satcomm_telemetry_short message
 *
 * @return Description of the field
 */
static inline uint16_t mavlink_msg_satcomm_telemetry_short_get_nav_bearing(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint16_t(msg,  16);
}

/**
 * @brief Get field ground_speed from satcomm_telemetry_short message
 *
 * @return Description of the field
 */
static inline uint16_t mavlink_msg_satcomm_telemetry_short_get_ground_speed(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint16_t(msg,  18);
}

/**
 * @brief Get field air_speed from satcomm_telemetry_short message
 *
 * @return Description of the field
 */
static inline uint16_t mavlink_msg_satcomm_telemetry_short_get_air_speed(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint16_t(msg,  20);
}

/**
 * @brief Get field bat_percent from satcomm_telemetry_short message
 *
 * @return Description of the field
 */
static inline uint8_t mavlink_msg_satcomm_telemetry_short_get_bat_percent(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint8_t(msg,  29);
}

/**
 * @brief Get field roll from satcomm_telemetry_short message
 *
 * @return Description of the field
 */
static inline int16_t mavlink_msg_satcomm_telemetry_short_get_roll(const mavlink_message_t* msg)
{
    return _MAV_RETURN_int16_t(msg,  22);
}

/**
 * @brief Get field pitch from satcomm_telemetry_short message
 *
 * @return Description of the field
 */
static inline int16_t mavlink_msg_satcomm_telemetry_short_get_pitch(const mavlink_message_t* msg)
{
    return _MAV_RETURN_int16_t(msg,  24);
}

/**
 * @brief Get field waypoint_num from satcomm_telemetry_short message
 *
 * @return Description of the field
 */
static inline int16_t mavlink_msg_satcomm_telemetry_short_get_waypoint_num(const mavlink_message_t* msg)
{
    return _MAV_RETURN_int16_t(msg,  26);
}

/**
 * @brief Decode a satcomm_telemetry_short message into a struct
 *
 * @param msg The message to decode
 * @param satcomm_telemetry_short C-struct to decode the message contents into
 */
static inline void mavlink_msg_satcomm_telemetry_short_decode(const mavlink_message_t* msg, mavlink_satcomm_telemetry_short_t* satcomm_telemetry_short)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    satcomm_telemetry_short->time = mavlink_msg_satcomm_telemetry_short_get_time(msg);
    satcomm_telemetry_short->latitude = mavlink_msg_satcomm_telemetry_short_get_latitude(msg);
    satcomm_telemetry_short->longitude = mavlink_msg_satcomm_telemetry_short_get_longitude(msg);
    satcomm_telemetry_short->altitude = mavlink_msg_satcomm_telemetry_short_get_altitude(msg);
    satcomm_telemetry_short->nav_bearing = mavlink_msg_satcomm_telemetry_short_get_nav_bearing(msg);
    satcomm_telemetry_short->ground_speed = mavlink_msg_satcomm_telemetry_short_get_ground_speed(msg);
    satcomm_telemetry_short->air_speed = mavlink_msg_satcomm_telemetry_short_get_air_speed(msg);
    satcomm_telemetry_short->roll = mavlink_msg_satcomm_telemetry_short_get_roll(msg);
    satcomm_telemetry_short->pitch = mavlink_msg_satcomm_telemetry_short_get_pitch(msg);
    satcomm_telemetry_short->waypoint_num = mavlink_msg_satcomm_telemetry_short_get_waypoint_num(msg);
    satcomm_telemetry_short->mode = mavlink_msg_satcomm_telemetry_short_get_mode(msg);
    satcomm_telemetry_short->bat_percent = mavlink_msg_satcomm_telemetry_short_get_bat_percent(msg);
#else
        uint8_t len = msg->len < MAVLINK_MSG_ID_SATCOMM_TELEMETRY_SHORT_LEN? msg->len : MAVLINK_MSG_ID_SATCOMM_TELEMETRY_SHORT_LEN;
        memset(satcomm_telemetry_short, 0, MAVLINK_MSG_ID_SATCOMM_TELEMETRY_SHORT_LEN);
    memcpy(satcomm_telemetry_short, _MAV_PAYLOAD(msg), len);
#endif
}
