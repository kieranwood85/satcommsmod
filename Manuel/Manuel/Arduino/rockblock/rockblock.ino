#include "mavlink\common\mavlink.h"
#include <Iridium.h>

Iridium isbd(Serial4, 11);

int state = 0, status;

unsigned long elapsedTime = 0;
unsigned long initialTime;

struct iridium_variables_t {
      int moStatus = -1;
      int momsn = -1;
      int mtStatus = -1;
      int mtmsn = -1;
      int mtLength = -1;
      int mtQueued = -1;
      int signalStrength = -1;
      int moFlag = -1;
      int mtFlag = -1;
      int raFlag = -1;
} iVars;

uint8_t message[] = "FINITE STATE MACHINE TEST 3";
uint8_t response[270];

int msgLength = 27;
int resLength = 270;

void setup() {

  pinMode(33, OUTPUT);
  digitalWrite(33, LOW);
  Serial.begin(9600);
  Serial4.begin(19200);

  delay(5000);

  initialTime = millis();

  Serial.println("IRIDIUM: SYSTEM STARTED");

}

void loop() {

  int status = isbd.update();
  elapsedTime = millis() - initialTime;

  switch(state) {

    case 0:
      if (elapsedTime > 60000) {
        Serial.println("IRIDIUM: ATTEMPTING NEW TRANSMISSION");
        if (isbd.doCSQ(&iVars.signalStrength) == Iridium::SUCCESS) state = 1;
      }
    break;

    case 1:
      if (status == Iridium::SUCCESS) state = 2;
      if (status == Iridium::FAILURE) state = 0;
    break;

    case 2:
      Serial.printf("IRIDIUM: SIGNAL STRENGTH = %d\n", iVars.signalStrength);
      if (iVars.signalStrength >= 3) state = 3;
      else state = 0;
    break;

    case 3:
      if (isbd.doSBDWB(message, msgLength) == Iridium::SUCCESS) state = 4;
    break;

    case 4:
      if (status == Iridium::SUCCESS) state = 5;
      if (status == Iridium::FAILURE) state = 0;
    break;

    case 5:
      Serial.println("IRIDIUM: CONNECTING...");
      if (isbd.doSBDIX(&iVars.moStatus, &iVars.momsn, &iVars.mtStatus, &iVars.mtmsn, &iVars.mtLength, &iVars.mtQueued) == Iridium::SUCCESS) state = 6;
    break;

    case 6:
      if (status == Iridium::SUCCESS) state = 7;
      if (status == Iridium::FAILURE) state = 0;
    break;

    case 7:
      if (iVars.moStatus == 0) state = 9;
      else state = 8;
    break;

    case 8:
      Serial.printf("IRIDIUM: CONNECTION FAILED WITH ERROR CODE %d\n", iVars.moStatus);
      state = 0;
    break;

    case 9:
      Serial.println("IRIDIUM: CONNECTION SUCCESSFUL");
      elapsedTime = 0;
      initialTime = millis();
      if (iVars.mtStatus == 1) state = 10;
      else state = 0;
    break;

    case 10:
      resLength = 270;
      if (isbd.doSBDRB(response, &resLength) == Iridium::SUCCESS) state = 11;
    break;

    case 11:
      if (status == Iridium::SUCCESS) state = 13;
      if (status == Iridium::FAILURE) state = 12;
    break;

    case 12:
      Serial.println("IRIDIUM: MESSAGE LOST!");
      state = 0;
    break;

    case 13:
      Serial.println("IRIDIUM: MESSAGE RECEIVED SUCCESSFULLY");
      Serial.print("IRIDIUM: ");
      Serial.write(response, resLength);
      Serial.println();
      state = 0;
    break;
    
  }

}
