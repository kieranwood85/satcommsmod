#include "mavlink\common\mavlink.h"

// Define the system type, in this case an airplane
int system_type = MAV_TYPE_FIXED_WING;
int autopilot_type = MAV_AUTOPILOT_GENERIC;

// Initialize the required buffers
uint8_t buf[MAVLINK_MAX_PACKET_LEN];
uint8_t sysID = 10;
uint8_t comID = 10;

uint32_t timestamp;
int32_t latitude = 110;
int32_t longitude = 200;
float altitude = 200.1245;
uint16_t nav_bearing = 360;
float nav_distance = 27.75;
uint16_t air_speed = 11;
uint8_t gps_fixtype = 0;
uint16_t bat_voltage = 123;
uint8_t bat_percent = 77;
uint16_t bat_current = 456;
int16_t roll = -90;
int16_t pitch = 90;
int16_t vertical_speed = 11;

uint16_t len;

int pressed = false;

void setup() {

  Serial1.begin(19200);
  pinMode(12, INPUT_PULLUP);
  delay(1000);

}

void loop() {

  if (!digitalRead(12)) pressed = true;
  
  if (pressed == true && digitalRead(12)) {

    Serial.println("Pressed");
    mavlink_message_t msg;
    timestamp = millis();
  
    memset(buf, 0, MAVLINK_MAX_PACKET_LEN);
    mavlink_msg_sat_telemetry_pack(sysID, comID, &msg, timestamp, latitude, longitude, altitude, nav_bearing, nav_distance, air_speed, gps_fixtype, bat_voltage, bat_percent, bat_current, roll, pitch, vertical_speed);
    len = mavlink_msg_to_send_buffer(buf, &msg);

    Serial.println();
    
    Serial1.write(buf, len);
    delay(100);
    pressed = false;

  }

}
