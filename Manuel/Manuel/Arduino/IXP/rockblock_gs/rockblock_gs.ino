//Include MAVLink and Iridium libraries
#include "mavlink\common\mavlink.h"
#include <Iridium.h>

//Assign serial ports for MAVLink and Iridium
HardwareSerial & mavlinkUART = Serial2;
HardwareSerial & iridiumUART = Serial3;

//Create a new Iridium object
Iridium isbd(iridiumUART, 11);

//Initialise the state and timing variables
int state = 0;
unsigned long elapsedTime = 0;
unsigned long initialTime;
bool transmitAwaiting = true;

//Structure of variables returned by the Iridium library
struct iridium_variables_t {
      int moStatus = -1;          //Transmitted message status
      int momsn = -1;             //Transmitted message sequence number
      int mtStatus = -1;          //Received message status
      int mtmsn = -1;             //Received message sequence number
      int mtLength = -1;          //Received message length
      int mtQueued = -1;          //Number of queued messages to receive
      int signalStrength = -1;    //Signal strength to the current satellite
      int moFlag = -1;            //Transmission buffer status
      int mtFlag = -1;            //Receive buffer status
      int raFlag = -1;            //Ring alert
};

//Set the address of the destination Rockblock: Serial Code 11067
uint8_t address[] = {82,66,0,43,59};

//Runs once on power up
void setup() {

  //Turn on the Rockblock
  pinMode(1, OUTPUT);
  digitalWrite(1, LOW);

  //Begin serial communications
  Serial.begin(19200);
  mavlinkUART.begin(19200);
  iridiumUART.begin(19200);

  //Delay for the Rockblock power up
  delay(5000);

  //Reset the timer
  initialTime = millis();

  Serial.println("IRIDIUM-GS: SYSTEM STARTED");

}

//Runs continually
void loop() {

  //Initialise the buffers used for sending and receiving data
  uint8_t receiveBuffer[270];
  uint8_t sendBuffer[340];
  int receiveLength = 0;
  int sendLength = 0;

  //Create a new instance of the Iridium structure
  iridium_variables_t iVars;

  //Internal infinite loop
  while (true) {  
    //Check for any incoming MAVLink messages over serial
    mavlinkReceive(sendBuffer, &sendLength);    
    //Handle communication with the Iridium modem                            
    iridiumUpdate(&iVars, sendBuffer, &sendLength, receiveBuffer, &receiveLength);
  }
  
}

//Transmit a MAVLink message over serial
void mavlinkTransmit(uint8_t *receiveBuffer, int sendLength) {

  //Initialise variables for MAVLink message and status
  mavlink_message_t msg;
  mavlink_status_t sts;

  //Initialise internal buffer for writing over serial
  uint8_t buf[340];
  memset(buf, '\0', 340);

  Serial.print("MAVLINK-GS: ");

  //Loop through the Iridium receive buffer
  for (int i = 0; i < sendLength; i++) {

    uint8_t c = receiveBuffer[i];
    Serial.print(c,HEX);

    //Parse the Iridium receive buffer for a MAVLink message
    if(mavlink_parse_char(MAVLINK_COMM_0, c, &msg, &sts)) {

      Serial.println();
      Serial.println("MAVLINK-GS: MESSAGE FORWARDED");

      //Write the MAVLink message over serial
      int len = mavlink_msg_to_send_buffer(buf, &msg);
      mavlinkUART.write(buf, len);
      
      Serial.print("Message ID: ");
      Serial.println(msg.msgid);
      
    }
    
  }

  return;
  
}

//Receive a MAVLink message over serial for satellite transmission
void mavlinkReceive(uint8_t *sendBuffer, int *sendLength) {

  //Initialise variables for MAVLink message and status
  mavlink_message_t msg;
  mavlink_status_t sts;

  //While data is available to read from the MAVLink serial buffer
  while(mavlinkUART.available() > 0) {

    uint8_t c = mavlinkUART.read();

    //Parse the incomming serial data for a MAVLink message
    if(mavlink_parse_char(MAVLINK_COMM_0, c, &msg, &sts)) {

      //Clear the Iridium send buffer and add the address data
      memset(sendBuffer, '\0', 340);
      memcpy(sendBuffer, address, 5);

      //Add the MAVLink binary data and set the transmit awaiting flag
      *sendLength = mavlink_msg_to_send_buffer(sendBuffer + 5, &msg) + 5;
      transmitAwaiting = true;
      break;  
    
    }
    
  }

  return;
  
}

//Main function for handling Iridium communications
void iridiumUpdate(struct iridium_variables_t *iVars, uint8_t *sendBuffer, int *sendLength, uint8_t *receiveBuffer, int *receiveLength) {

  //Run an update of the Iridium object and obtain the returned status
  int status = isbd.update();
  //Update the current time
  elapsedTime = millis() - initialTime;

  //Finite state machine for Iridium communications
  switch(state) {

    //Order the Iridium object to begin a request for detailed status information from the modem
    case 0:
      if (isbd.doSBDSX(&iVars->moFlag, &iVars->momsn, &iVars->mtFlag, &iVars->mtmsn, &iVars->raFlag, &iVars->mtQueued) == Iridium::SUCCESS) state = 1;
    break;

    //Wait for the job to complete successfully or to fail
    case 1:
      if (status == Iridium::SUCCESS) state = 2;
      if (status == Iridium::FAILURE) state = 0;
    break;

    //If a transmission is awaiting or a message needs to be received order the Iridium object to begin a request for signal strength
    case 2:
      if (elapsedTime > 300000 || (elapsedTime > 30000 && transmitAwaiting) || iVars->raFlag == 1 || iVars->mtQueued > 0) {
        Serial.println("IRIDIUM-GS: ATTEMPTING NEW RECEIVE");
        Serial.println("IRIDIUM-GS: OBTAINING CURRENT SIGNAL STRENGTH");
        if (isbd.doCSQ(&iVars->signalStrength) == Iridium::SUCCESS) state = 3;
      }
      else state = 0;
    break;

    //Wait for the job to complete successfully or to fail
    case 3:
      if (status == Iridium::SUCCESS) { Serial.println("IRIDIUM-GS: SUCCESSFULLY OBTAINED SIGNAL STRENGTH"); state = 4; }
      if (status == Iridium::FAILURE) { Serial.println("IRIDIUM-GS: FAILED TO GET SIGNAL STRENGTH"); state = 0; }
    break;

    //If the signal strength is greater than or equal to 3 proceed, else return to the first state
    case 4:
      Serial.printf("IRIDIUM-GS: SIGNAL STRENGTH = %d\n", iVars->signalStrength);
      if (iVars->signalStrength >= 3) {
        if (*sendLength > 0) state = 5;
        else state = 7;
      }
      else state = 0;
    break;

    //Order the Iridium object to begin writing a new binary message to the modem
    case 5:
      Serial.println("IRIDIUM-GS: WRITING MESSAGE TO TRANSMISSION BUFFER");
      if (isbd.doSBDWB(sendBuffer, *sendLength) == Iridium::SUCCESS) state = 6;
    break;

    //Wait for the job to complete successfully or to fail
    case 6:
      if (status == Iridium::SUCCESS) { Serial.println("IRIDIUM-GS: BINARY MESSAGE WRITTEN SUCCESSFULLY"); state = 7; }
      if (status == Iridium::FAILURE) { Serial.println("IRIDIUM-GS: FAILED TO WRITE BINARY MESSAGE"); state = 0; }
    break;

    //Order the Iridium object to begin a satellite transfer, using SBDIXA if a ring alter is present
    case 7:
      Serial.println("IRIDIUM-GS: CONNECTING...");
      if (iVars->raFlag == 1) {
        if (isbd.doSBDIXA(&iVars->moStatus, &iVars->momsn, &iVars->mtStatus, &iVars->mtmsn, &iVars->mtLength, &iVars->mtQueued) == Iridium::SUCCESS) state = 8;
      }
      else if (isbd.doSBDIX(&iVars->moStatus, &iVars->momsn, &iVars->mtStatus, &iVars->mtmsn, &iVars->mtLength, &iVars->mtQueued) == Iridium::SUCCESS) state = 8;
    break;

    //Wait for the job to complete successfully or to fail
    case 8:
      if (status == Iridium::SUCCESS) state = 9;
      if (status == Iridium::FAILURE) { Serial.println("IRIDIUM-GS: SATELLITE TRANSFER ERROR"); state = 0; }
    break;

    //If the communication was successful proceed, else return to the first state
    case 9:
      if (iVars->moStatus >= 0 && iVars->moStatus <= 4) state = 11;
      else state = 10;
    break;

    //Error result for satellite transfer
    case 10:
      Serial.printf("IRIDIUM-GS: CONNECTION FAILED WITH ERROR CODE %d\n", iVars->moStatus);
      state = 0;
    break;

    //Reset transmission variables and the timer, then order the Iridium object to command clearing of the transmit buffer
    case 11:
      Serial.println("IRIDIUM-GS: CONNECTION SUCCESSFUL");
      Serial.println("IRIDIUM-GS: ATTEMPTING TO CLEAR TRANSMISSION BUFFER");
      transmitAwaiting = false;
      *sendLength = 0;
      elapsedTime = 0;
      initialTime = millis();
      if (isbd.doSBDD(0) == Iridium::SUCCESS) state = 12;
    break;

    //Wait for the job to complete successfully or to fail
    case 12:
      if (status == Iridium::SUCCESS) { Serial.println("IRIDIUM-GS: TRANSMISSION BUFFER CLEARED SUCCESSFULLY"); state = 13; }
      if (status == Iridium::FAILURE) { Serial.println("IRIDIUM-GS: FAILED TO CLEAR TRANSMISSION BUFFER"); state = 0; }
    break;

    //Check whether there a message was receive during the latest satellite transfer
    case 13:
      if (iVars->mtStatus == 1) state = 14;
      else { Serial.println("IRIDIUM-GS: COMPLETED TRANSFER"); state = 0; }
    break;

    //Order the Iridium object to begin reading a new binary message from the modem
    case 14:
      *receiveLength = 270;
      Serial.println("IRIDIUM-GS: ATTEMPTING TO READ RECEIVED MESSAGE");
      if (isbd.doSBDRB(receiveBuffer, receiveLength) == Iridium::SUCCESS) state = 15;
    break;

    //Wait for the job to complete successfully or to fail
    case 15:
      if (status == Iridium::SUCCESS) state = 17;
      if (status == Iridium::FAILURE) state = 16;
    break;

    //Error result for binary read
    case 16:
      Serial.println("IRIDIUM-GS: SEVERE ERROR MESSAGE LOST");
      state = 0;
    break;

    //Check the received data for MAVLinks and transmit them over serial
    case 17:
      Serial.println("IRIDIUM-GS: MESSAGE RECEIVED SUCCESSFULLY");
      Serial.println("IRIDIUM-GS: COMPLETED TRANSFER");
      mavlinkTransmit(receiveBuffer, *receiveLength);
      state = 0;
    break;
    
  }

  return;

}


