"""
This recipe describes how to handle asynchronous I/O in an environment where
you are running Tkinter as the graphical user interface. Tkinter is safe
to use as long as all the graphics commands are handled in a single thread.
Since it is more efficient to make I/O channels to block and wait for something
to happen rather than poll at regular intervals, we want I/O to be handled
in separate threads. These can communicate in a threasafe way with the main,
GUI-oriented process through one or several queues. In this solution the GUI
still has to make a poll at a reasonable interval, to check if there is
something in the queue that needs processing. Other solutions are possible,
but they add a lot of complexity to the application.

Created by Jacob Hallén, AB Strakt, Sweden. 2001-10-17
"""
import tkinter
from tkinter import messagebox, PhotoImage, filedialog
from tkinter import ttk

import time
import threading
from collections import defaultdict

import csv

import queue

import serial
import serial.tools.list_ports

from struct import unpack
import sys

SERIAL_PORT = 'COM45'
SERIAL_BAUD = 115200
LABEL_PADX = 10
LABEL_PADY = 3

FRAME_PADX = 10
FRAME_PADY = 15

try:
    ser = serial.Serial(SERIAL_PORT,SERIAL_BAUD,
        parity=serial.PARITY_EVEN,
        stopbits=serial.STOPBITS_ONE,
        bytesize=serial.EIGHTBITS,
        timeout=2)
    print(ser.name)
except:
    pass

    


GPS_lat = 0.00
GPS_long=0.00

#last_packet_time = time.clock()

class GuiPart:
    def __init__(self, master, queue, endCommand):
        self.queue = queue
        # Set up the GUI
        self.writer = None
        self.root = root
        root.geometry('760x400')
        root.title("Satcom GCS v1.0.2 ")

        menu = tkinter.Menu(root)
        menu.add_command(label="File")
        menu.add_command(label="About", command=clicked_about)
        root.config(menu=menu)
        self.last_packet_time=0

        #self.Time_label =  tkinter.Label(root, text="-", borderwidth=1, relief="sunken",bg="white", width=100)
        #self.Time_label.grid(column=1, row=0, pady=LABEL_PADY+20, padx=LABEL_PADX,columnspan = 2)
        #self.updateClock()
        
        # FRAAME 1: BEACON DATA

        self.FrameBeacon = tkinter.LabelFrame(root, text='Beacon', relief=tkinter.GROOVE)
        self.FrameBeacon.grid(column=1,row=1, pady=FRAME_PADY, padx=FRAME_PADX,sticky = tkinter.N)
        
        self.bitmask1_label = tkinter.Label(self.FrameBeacon, text="Pixhawk")
        self.bitmask1_label.grid(column=1, row=0, pady=LABEL_PADY, padx=LABEL_PADX,sticky = tkinter.W)
        self.bitmask1 = tkinter.Label(self.FrameBeacon, text="No Connection", borderwidth=1, relief="sunken",bg="red", width=30)
        self.bitmask1.grid(column=2, row=0, pady=LABEL_PADY, padx=LABEL_PADX)


        self.bitmask2_label = tkinter.Label(self.FrameBeacon, text="GPS fix")
        self.bitmask2_label.grid(column=1, row=1, pady=LABEL_PADY, padx=LABEL_PADX,sticky = tkinter.W)
        self.bitmask2 = tkinter.Label(self.FrameBeacon, text="No Fix", borderwidth=1, relief="sunken",bg="yellow", width=30)
        self.bitmask2.grid(column=2, row=1, pady=LABEL_PADY, padx=LABEL_PADX)
        
        self.GPS_lat_label = tkinter.Label(self.FrameBeacon, text="GPS Lat")
        self.GPS_lat_label.grid(column=1, row=2, pady=LABEL_PADY, padx=LABEL_PADX,sticky = tkinter.W)
        self.GPS_lat = tkinter.Label(self.FrameBeacon, text="-", borderwidth=1, relief="sunken",bg="white", width=30)
        self.GPS_lat.grid(column=2, row=2, pady=LABEL_PADY, padx=LABEL_PADX)

        self.GPS_lon_label = tkinter.Label(self.FrameBeacon, text="GPS Lon")
        self.GPS_lon_label.grid(column=1, row=3, pady=LABEL_PADY, padx=LABEL_PADX,sticky = tkinter.W)
        self.GPS_lon = tkinter.Label(self.FrameBeacon, text="-", borderwidth=1, relief="sunken",bg="white", width=30)
        self.GPS_lon.grid(column=2, row=3, pady=LABEL_PADY, padx=LABEL_PADX)

        self.GPS_alt_label = tkinter.Label(self.FrameBeacon, text="GPS Alt [m]")
        self.GPS_alt_label.grid(column=1, row=4, pady=LABEL_PADY, padx=LABEL_PADX,sticky = tkinter.W)
        self.GPS_alt = tkinter.Label(self.FrameBeacon, text="-", borderwidth=1, relief="sunken",bg="white", width=30)
        self.GPS_alt.grid(column=2, row=4, pady=LABEL_PADY, padx=LABEL_PADX)

        self.SatVbatt_label = tkinter.Label(self.FrameBeacon, text="Beacon battery [V]")
        self.SatVbatt_label.grid(column=1, row=5, pady=LABEL_PADY, padx=LABEL_PADX,sticky = tkinter.W)
        self.SatVbatt = tkinter.Label(self.FrameBeacon, text="-", borderwidth=1, relief="sunken",bg="white", width=30)
        self.SatVbatt.grid(column=2, row=5, pady=LABEL_PADY, padx=LABEL_PADX)

        # FRAME 2: UAS DATA

        self.FrameUAS = tkinter.LabelFrame(root, text='Mavlink', relief=tkinter.GROOVE)
        self.FrameUAS.grid(column=2,row=1, pady=FRAME_PADY, padx=FRAME_PADX,sticky = tkinter.N)

        self.CurrentWaypoint_label = tkinter.Label(self.FrameUAS, text="Current Waypoint")
        self.CurrentWaypoint_label.grid(column=1, row=1, pady=LABEL_PADY, padx=LABEL_PADX,sticky = tkinter.W)
        self.CurrentWaypoint = tkinter.Label(self.FrameUAS, text="-", borderwidth=1, relief="sunken",bg="white", width=30)
        self.CurrentWaypoint.grid(column=2, row=1, pady=LABEL_PADY, padx=LABEL_PADX)

        self.airspeed_label = tkinter.Label(self.FrameUAS, text="Airspeed [m/s]")
        self.airspeed_label.grid(column=1, row=2, pady=LABEL_PADY, padx=LABEL_PADX,sticky = tkinter.W)
        self.airspeed = tkinter.Label(self.FrameUAS, text="-", borderwidth=1, relief="sunken",bg="white", width=30)
        self.airspeed.grid(column=2, row=2, pady=LABEL_PADY, padx=LABEL_PADX)

        self.alt_label = tkinter.Label(self.FrameUAS, text="Alt [m]")
        self.alt_label.grid(column=1, row=3, pady=LABEL_PADY, padx=LABEL_PADX,sticky = tkinter.W)
        self.alt = tkinter.Label(self.FrameUAS, text="-", borderwidth=1, relief="sunken",bg="white", width=30)
        self.alt.grid(column=2, row=3, pady=LABEL_PADY, padx=LABEL_PADX)

        self.climb_label = tkinter.Label(self.FrameUAS, text="Climb [m/s]")
        self.climb_label.grid(column=1, row=4, pady=LABEL_PADY, padx=LABEL_PADX,sticky = tkinter.W)
        self.climb = tkinter.Label(self.FrameUAS, text="-", borderwidth=1, relief="sunken",bg="white", width=30)
        self.climb.grid(column=2, row=4, pady=LABEL_PADY, padx=LABEL_PADX)

        self.heading_label = tkinter.Label(self.FrameUAS, text="Heading [deg]")
        self.heading_label.grid(column=1, row=5, pady=LABEL_PADY, padx=LABEL_PADX,sticky = tkinter.W)
        self.heading = tkinter.Label(self.FrameUAS, text="-", borderwidth=1, relief="sunken",bg="white", width=30)
        self.heading.grid(column=2, row=5, pady=LABEL_PADY, padx=LABEL_PADX)

        self.throttle_label = tkinter.Label(self.FrameUAS, text="Throttle [%]")
        self.throttle_label.grid(column=1, row=6, pady=LABEL_PADY, padx=LABEL_PADX,sticky = tkinter.W)
        self.throttle = tkinter.Label(self.FrameUAS, text="-", borderwidth=1, relief="sunken",bg="white", width=30)
        self.throttle.grid(column=2, row=6, pady=LABEL_PADY, padx=LABEL_PADX)

        self.UAV_battery_label = tkinter.Label(self.FrameUAS, text="UAV battery [V]")
        self.UAV_battery_label.grid(column=1, row=7, pady=LABEL_PADY, padx=LABEL_PADX,sticky = tkinter.W)
        self.UAV_battery = tkinter.Label(self.FrameUAS, text="-", borderwidth=1, relief="sunken",bg="white", width=30)
        self.UAV_battery.grid(column=2, row=7, pady=LABEL_PADY, padx=LABEL_PADX)

        # FRAME 2: SERIAL CONNECTION

        self.SerialConnection = tkinter.LabelFrame(root, text='Serial config', relief=tkinter.GROOVE)
        self.SerialConnection.grid(column=1,row=2, pady=FRAME_PADY, padx=FRAME_PADX, columnspan = 2, sticky = tkinter.N+tkinter.W)

        self.serialPort_label = tkinter.Label(self.SerialConnection, text="Serial port:")
        self.serialPort_label.grid(column=1, row=1, pady=LABEL_PADY, padx=LABEL_PADX)
        
        self.serialPortSelect = ttk.Combobox(self.SerialConnection, values=serial_ports(),width=30)
        self.serialPortSelect.grid(column=2, row=1, pady=LABEL_PADY, padx=LABEL_PADX)
        self.serialPortSelect.bind('<<ComboboxSelected>>', self.on_select)

        #self.Time_label =  tkinter.Label(self.SerialConnection, text="-", borderwidth=1, relief="sunken",bg="white", width=100)
        #self.Time_label.grid(column=1, row=2, pady=LABEL_PADY, padx=LABEL_PADX,columnspan = 2)
        #self.updateClock()

        self.saveasButton = tkinter.Button(root,text="Save To File", command = self.select_save_file)
        self.saveasButton.grid(column=1,row=3, pady=LABEL_PADY, padx=LABEL_PADX)

        #self.photo = PhotoImage(file="UoBlogo.gif")
        #self.Logo =  tkinter.Label(root, image = self.photo)
        #self.Logo.grid(column=1, row=0)

    def select_save_file(self):
        delfaultFilename = "logs_"+time.strftime("%Y%m%d-%H%M%S")
        self.saveFile = filedialog.asksaveasfile(initialfile=delfaultFilename,title = 'Save logs',mode='w', defaultextension=".csv",filetypes=[("CSV (Comma Delimited)","*.csv"),("All files","*.*")],confirmoverwrite=True)
        if self.saveFile is None: # asksaveasfile return `None` if dialog closed with "cancel".
            return
        with open(self.saveFile.name, 'w',newline="") as csvfile:
            self.fieldnamesList = ['timestamp','bitmask1', 'bitmask2', 'GPS_lat', 'GPS_long', 'GPS_alt', 'SatVbatt', 'CurrentWaypoint', 'airspeed', 'Alt', 'climb', 'heading', 'throttle', 'UAV_battery']
            self.writer = csv.DictWriter(csvfile, fieldnames=self.fieldnamesList,quoting=csv.QUOTE_ALL)
            self.writer.writeheader()

    def on_select(self, event=None):
        global ser
        ser = serial.Serial(self.serialPortSelect.get(),SERIAL_BAUD,
            parity=serial.PARITY_EVEN,
            stopbits=serial.STOPBITS_ONE,
            bytesize=serial.EIGHTBITS,
            timeout=2)
        
    def updateClock(self):
        seconds_since_last_packet =  time.clock() - self.last_packet_time
        self.Time_label.configure(text= "Last packet: {0:.0f} seconds ago".format(seconds_since_last_packet))
        self.root.after(1000, self.updateClock)
        
    def processIncoming(self):
        """
        Handle all the messages currently in the queue (if any).
        """
        while self.queue.qsize():
            try:
                msg = self.queue.get(0)
                #save msg to file
                if self.writer is not None:
                    namesDict=dict(zip(self.fieldnamesList,msg))
                    with open(self.saveFile.name, 'a',newline="") as csvfile:
                        self.writer = csv.DictWriter(csvfile, fieldnames=self.fieldnamesList,quoting=csv.QUOTE_ALL)
                        self.writer.writerow(namesDict)
                    
                if msg[2] == 0:
                    self.bitmask2.configure(text= "No Fix")
                    self.bitmask2.config(bg="yellow")
                    self.GPS_lat.config(bg="yellow")
                    self.GPS_lon.config(bg="yellow")
                    self.GPS_alt.config(bg="yellow")
                else:
                    self.bitmask2.configure(text= "3D Fix")
                    self.bitmask2.config(bg="white")
                    self.GPS_lat.config(bg="white")
                    self.GPS_lon.config(bg="white")
                    self.GPS_alt.config(bg="white")

                GPS_parsed = NMEA_to_latlong(msg[3], msg[4])    
                self.GPS_lat.configure(text= "{0:2.6f}".format(GPS_parsed[0]))
                self.GPS_lon.configure(text= "{0:3.6f}".format(GPS_parsed[1]))
                self.GPS_alt.configure(text= "{0:6.2f}".format(msg[5]))
                self.SatVbatt.configure(text= "{0:4.2f}".format(msg[6]))
                self.CurrentWaypoint.configure(text= msg[7])
                self.airspeed.configure(text= "{0:6.2f}".format(msg[8]))
                self.alt.configure(text= "{0:6.2f}".format(msg[9]))
                self.climb.configure(text= "{0:6.2f}".format(msg[10]))
                self.heading.configure(text= "{0:6.2f}".format(msg[11]))
                self.throttle.configure(text= msg[12])
                self.UAV_battery.configure(text= "{0:4.2f}".format(msg[13]))
                
                self.last_packet_time = time.clock()
                
                if msg[1] == 0:
                    self.bitmask1.config(bg="red")
                    self.bitmask1.configure(text= "No connection")
                    
                    self.CurrentWaypoint.config(bg="red")
                    self.airspeed.config(bg="red")
                    self.alt.config(bg="red")
                    self.climb.config(bg="red")
                    self.heading.config(bg="red")
                    self.throttle.config(bg="red")
                    self.UAV_battery.config(bg="red")
                else:
                    self.bitmask1.configure(text= "Connected")
                    self.bitmask1.config(bg="white")
                    self.CurrentWaypoint.config(bg="white")
                    self.airspeed.config(bg="white")
                    self.alt.config(bg="white")
                    self.climb.config(bg="white")
                    self.heading.config(bg="white")
                    self.throttle.config(bg="white")
                    self.UAV_battery.config(bg="white")
            except queue.Empty:
                pass

class ThreadedClient:
    """
    Launch the main part of the GUI and the worker thread. periodicCall and
    endApplication could reside in the GUI part, but putting them here
    means that you have all the thread controls in a single place.
    """
    def __init__(self, master):
        """
        Start the GUI and the asynchronous threads. We are in the main
        (original) thread of the application, which will later be used by
        the GUI. We spawn a new thread for the worker.
        """
        self.master = master

        # Create the queue
        self.queue =queue.Queue()

        # Set up the GUI part
        self.gui = GuiPart(master, self.queue, self.endApplication)

        # Set up the thread to do asynchronous I/O
        # More can be made if necessary
        self.running = 1
        self.thread1 = threading.Thread(target=self.workerThread1)
        self.thread1.start()

        # Start the periodic call in the GUI to check if the queue contains
        # anything
        self.periodicCall()

    def periodicCall(self):
        """
        Check every 100 ms if there is something new in the queue.
        """
        self.gui.processIncoming()
        if not self.running:
            # This is the brutal stop of the system. You may want to do
            # some cleanup before actually shutting it down.
            import sys
            sys.exit(1)
        self.master.after(100, self.periodicCall)

    def workerThread1(self):
        """
        This is where we handle the asynchronous I/O. For example, it may be
        a 'select()'.
        One important thing to remember is that the thread has to yield
        control.
        """
        while self.running:
            # To simulate asynchronous I/O, we create a random number at
            # random intervals. Replace the following 2 lines with the real
            # thing.
            #time.sleep(rand.random() * 0.3)
            try:
                msg = [0,0,0,0,0,0,0,0,0,0,0,0,0]
                leadingByte = ser.read(1)
                if leadingByte == b'\x02': #STX
                    data = ser.read(46)
                    trailingByte = ser.read(1)
                    if trailingByte ==b'\x03':      #ETX
                        #bitmask, GPS_lat, GPS_long = unpack('=hff',data)
                        #msg = [bitmask, GPS_lat, GPS_long]
                        timestamp =time.strftime("%Y%m%d-%H%M%S")
                        bitmask1, bitmask2, GPS_lat, GPS_long, GPS_alt, SatVbatt, CurrentWaypoint, airspeed, alt, climb, heading, throttle, UAV_battery = unpack('=hhffffhffffff',data)
                        msg = [timestamp,bitmask1, bitmask2, GPS_lat, GPS_long, GPS_alt, SatVbatt, CurrentWaypoint, airspeed, alt, climb, heading, throttle, UAV_battery]
                        #print(*msg, sep='\n')
                        self.queue.put(msg)
            except NameError:
                msg = [0,0,0,0,0,0,0,0,0,0,0,0,0]

    def endApplication(self):
        if self.saveFile is not None:
            self.saveFile.close()
        self.running = 0

def clicked_about():
        messagebox.showinfo('About', '\xa9 2019 Manuel Martinez \n All Rights Reserved \n Contact: mm16356@bristol.ac.uk')

def NMEA_to_latlong(NMEA_lat, NMEA_lon):
    def sign(x):
        if x > 0:
            return 1.
        elif x < 0:
            return -1.
        elif x == 0:
            return 0.
        else:
            return x
    lat_deg = (int(abs(NMEA_lat)/100) + (abs(NMEA_lat) % 100)/60) * sign(NMEA_lat)
    lon_deg = (int(abs(NMEA_lon)/100) + (abs(NMEA_lon) % 100)/60) * sign(NMEA_lon)
    output = [lat_deg,lon_deg]
    return output

def serial_ports():
    ports = serial.tools.list_ports.comports()
    for port_no, description, address in ports:
        if 'USB' in description:
            return port_no






root = tkinter.Tk()



root.iconbitmap("favicon.ico")
client = ThreadedClient(root)
root.mainloop()
