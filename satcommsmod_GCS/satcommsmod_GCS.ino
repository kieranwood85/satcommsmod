

/*********************************************************************

  Copyright (c) 2018-2019 M. Martinez - All Rights Reserved

  File title: satcommsmod_GCS Alpha v0.1

  Developers:
  M. Martinez (mm16356@bristol.ac.uk)


  --------------PIN ASSIGNMENT-------------------------------
                      ARDUINO (IDE)      FEATHER (SILKMASK)
                    ---------------------------------------
  IRIDIUM TX          10                 10
  IRIDIUM RX          11                 11
  IRIDIUM SLEEP       1                  PNK
  IRIDIUM RING        5?                 GRN
  ----------------------------------------------------------
  *********************************************************************/

#include <IridiumSBD.h>
#include <time.h>
 
#define IridiumSerial Serial3
#define RING_PIN 0
#define SLEEP_PIN 1
#define DIAGNOSTICS false // Change this to see diagnostics

IridiumSBD modem(IridiumSerial, SLEEP_PIN, RING_PIN);

void setup()
{
  int signalQuality = -1;

  // Start the serial ports
  Serial.begin(115200);
  while (!Serial);
  IridiumSerial.begin(19200);

  // Setup the Iridium modem
  modem.setPowerProfile(IridiumSBD::USB_POWER_PROFILE);
  if (modem.begin() != ISBD_SUCCESS)
  {
    Serial.println("Couldn't begin modem operations.");
    exit(0);
  }

  // Check signal quality for fun.
  int err = modem.getSignalQuality(signalQuality);
  if (err != 0)
  {
    Serial.print("SignalQuality failed: error ");
    Serial.println(err);
    return;
  }

  Serial.print("Signal quality is ");
  Serial.println(signalQuality);
  Serial.println("Begin waiting for RING...");
}


void loop()
{
  static int err = ISBD_SUCCESS;
  bool ring = modem.hasRingAsserted();
  if (ring || modem.getWaitingMessageCount() > 0 || err != ISBD_SUCCESS)
  {
    if (ring)
      Serial.println("RING asserted!  Let's try to read the incoming message.");
    else if (modem.getWaitingMessageCount() > 0)
      Serial.println("Waiting messages available.  Let's try to read them.");
    else
      Serial.println("Let's try again.");

    uint8_t buffer[200];
    size_t bufferSize = sizeof(buffer);
    err = modem.sendReceiveSBDText(NULL, buffer, bufferSize);
    if (err != ISBD_SUCCESS)
    {
      Serial.print("sendReceiveSBDBinary failed: error ");
      Serial.println(err);
      return;
    }

    Serial.println("Message received!");
    Serial.print("Inbound message size is ");
    Serial.println(bufferSize);
    for (int i=0; i<(int)bufferSize; ++i)
    {
      Serial.print(buffer[i], HEX);
      if (isprint(buffer[i]))
      {
        Serial.print("(");
        Serial.write(buffer[i]);
        Serial.print(")");
      }
      Serial.print(" ");
    }
    Serial.println();
    Serial.print("Messages remaining to be retrieved: ");
    Serial.println(modem.getWaitingMessageCount());
  }
}

#if DIAGNOSTICS
void ISBDConsoleCallback(IridiumSBD *device, char c)
{
  Serial.write(c);
}

void ISBDDiagsCallback(IridiumSBD *device, char c)
{
  Serial.write(c);
}
#endif
