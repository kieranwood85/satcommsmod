/*********************************************************************

Copyright (c) 2018 M. Martinez - All Rights Reserved

File title: Satellite Modem Send test
Developers:
  M. Martinez (mm16356@bristol.ac.uk)
  

--------------PIN ASSIGNMENT-------------------------------
                      ARDUINO (IDE)      FEATHER (SILKMASK)
                    ---------------------------------------
  IRIDIUM TX          10                 10  
  IRIDIUM RX          11                 11
  IRIDIUM SLEEP       9                  9
  IRIDIUM RING        12                 12
  VBATT INTERNAL      A7 (hardwired)     9
  VBATT EXTERNAL      A5                 TBD
  GPS TX              N/A (Serial1)      TX1
  GPS RX              N/A (Serial1)      RX0 
  PIXHAWK TX          23                 MOSI
  PIXHAWK RX          22                 MISO
  PIXHAWK RTS         TBD                TBD
  PIXHAWK CTS         TBD                TBD
  ----------------------------------------------------------
  *********************************************************************/

#include <Arduino.h>
#include <string.h>
#include "wiring_private.h" 
#include <IridiumSBD.h>
//.ñup#include "mavlinkHeaders\ardupilotmega\mavlink.h"  //For serial messages to the pixhawk


#define USBSerial     Serial
#define GPSSerial     Serial1
#define IridiumSerial Serial2
#define MAVLinkSerial Serial3
#define DIAGNOSTICS true // Change this to see diagnostics


// Arduino pin assignment (1/2)
//Config SERCOM1 as Serial3 on pins 11, 10 for Iridium
Uart Serial2 (&sercom1, 11, 10, SERCOM_RX_PAD_0, UART_TX_PAD_2);
void SERCOM1_Handler()
{
  Serial2.IrqHandler();
}

//Config SERCOM4 as Serial3 on pins MISO, MOSI for Pixhawk
Uart Serial3 (&sercom4, 22, 23, SERCOM_RX_PAD_0, UART_TX_PAD_2);
void SERCOM4_Handler()
{
  Serial3.IrqHandler();
}

// Declare the Objects 
IridiumSBD modem(IridiumSerial);

//Declare variables
uint32_t timer = millis();
int signalQuality = -1;
int err;

void setup()
{

  // Start the console serial port
  USBSerial.begin(115200);
  while (!USBSerial);


  // Start the serial port connected to the satellite modem
  IridiumSerial.begin(19200);


  //Start connection to Pixhawk
  MAVLinkSerial.begin(57600);

  //Arduino pin assignment (2/2)
  // Assign pins 10 & 11 SERCOM1 functionality (Seria2)
  pinPeripheral(10, PIO_SERCOM);  //TX (10)
  pinPeripheral(11, PIO_SERCOM);  //RX (11)
  // Assign pins MISO & MOSI SERCOM4 functionality (Serial3)
  pinPeripheral(22, PIO_SERCOM_ALT); //RX (MISO on board)
  pinPeripheral(23, PIO_SERCOM_ALT); //TX (MOSI on board)

 
  // Begin satellite modem operation
  Serial.println("Starting modem...");
  err = modem.begin();
  if (err != ISBD_SUCCESS)
  {
    Serial.print("Begin failed: error ");
    Serial.println(err);
    if (err == ISBD_NO_MODEM_DETECTED)
      Serial.println("No modem detected: check wiring.");
    return;
  }

  // Example: Print the firmware revision
  char version[12];
  err = modem.getFirmwareVersion(version, sizeof(version));
  if (err != ISBD_SUCCESS)
  {
     Serial.print("FirmwareVersion failed: error ");
     Serial.println(err);
     return;
  }
  Serial.print("Firmware Version is ");
  Serial.print(version);
  Serial.println(".");

  // Example: Test the signal quality.
  // This returns a number between 0 and 5.
  // 2 or better is preferred.
  err = modem.getSignalQuality(signalQuality);
  if (err != ISBD_SUCCESS)
  {
    Serial.print("SignalQuality failed: error ");
    Serial.println(err);
    return;
  }

  Serial.print("On a scale of 0 to 5, signal quality is currently ");
  Serial.print(signalQuality);
  Serial.println(".");

  // Send the message
  /*
  Serial.print("Trying to send the message.  This might take several minutes.\r\n");
  err = modem.sendSBDText("Nobody expects the Spanish Inquisition!");
  if (err != ISBD_SUCCESS)
  {
    Serial.print("sendSBDText failed: error ");
    Serial.println(err);
    if (err == ISBD_SENDRECEIVE_TIMEOUT)
      Serial.println("Timeout error, try better view of sky.");
  }

  else
  {
    Serial.println("MSG Sent!");
  }
  */
}

void loop()
{

 
}

#if DIAGNOSTICS
void ISBDConsoleCallback(IridiumSBD *device, char c)
{
  Serial.write(c);
}

void ISBDDiagsCallback(IridiumSBD *device, char c)
{
  Serial.write(c);
}
#endif
