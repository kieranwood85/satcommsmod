REM Script to create the MAVLink headers.
REM cd to this directory then call this .bat from the command link
C:\ProgramData\Anaconda2\python.exe modules\mavlink\pymavlink\tools\mavgen.py --lang=C --wire-protocol=2.0 -o mavlinkHeaders modules\mavlink\message_definitions\v1.0\ardupilotmega.xml