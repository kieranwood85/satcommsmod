/*********************************************************************

  Copyright (c) 2018-2019 M. Martinez - All Rights Reserved

  File title: satcommsmod Alpha v0.7

  Developers:
  M. Martinez (mm16356@bristol.ac.uk)
  Developer:
  K. Wood     (kw4064@bristol.ac.uk)


  --------------PIN ASSIGNMENT-------------------------------
                      ARDUINO (IDE)      FEATHER (SILKMASK)
                    ---------------------------------------
  IRIDIUM TX          10                 10
  IRIDIUM RX          11                 11
  IRIDIUM SLEEP       9                  9
  IRIDIUM RING        12                 12
  VBATT INTERNAL      A7 (hardwired)     9
  VBATT EXTERNAL      A5                 A5
  GPS TX              N/A (Serial1)      TX1
  GPS RX              N/A (Serial1)      RX0
  PIXHAWK TX          23                 MOSI
  PIXHAWK RX          22                 MISO
  PIXHAWK RTS         TBD                TBD
  PIXHAWK CTS         TBD                TBD
  ----------------------------------------------------------
  *********************************************************************/

  //TO-DO: Make int vs uint homogeneous

#include <Arduino.h>
#include <string.h>                                //For feather I/O
#include "wiring_private.h"                        //For Feather I/O
#include <Adafruit_GPS.h>                          //For GPS shield support
#include "mavlinkHeaders\ardupilotmega\mavlink.h"  //For serial messages to the pixhawk
#include <IridiumSBD.h>                            //For Iridium Satcom support

// Define serial port alias
#define USBSerial     Serial
#define GPSSerial     Serial1
#define IridiumSerial Serial2
#define MAVLinkSerial Serial3

// HW configuration
#define ADCMULTIPLIER_VBAT 0.0064453 // =/volt_div(0.5) *ref_volt(3.3) /_range(1024)
#define PIN_VBATT_INTERNAL A7

//SATCOM configuration
#define PACKET_SIZE 48  //Try to keep message short (48 byte=1block, so sent all in one go)
#define STX_BITE 0x02
#define ETX_BITE 0x03

// Debug config
#define WAIT_FOR_USB_CONNECTION false   // Change this to wait for serial monitor before executing setup (DOES NOT ENABLE DATA BY ITSELF! Need serial_verbose for that)
#define SERIAL_VERBOSE false


// Change this to get some debug info through serial port
#define IRIDIUM_DIAGNOSTICS true        // Change this to see iridium sat modem diagnostics
#define GPS_VERBOSE_5_SEC false         // Change this to get 1/5 Hz GPS data trough serial port
#define GPS_NMEA_ECHO false             // Change this to get raw NMEA sentences through serial port 
//TO-DO IMPLEMENT #define GPS_DEBUG_USE_MOCK_DATA false   // Change this to use mock GPS data (useful when indoors)

// THIS BLOCK OF CODE ENABLES CONSOLE (SERIAL PORT) DEBUG MESSAGES. DONE THIS WAY
// (INSTEAD OF INDIVIDUAL IF STATMENTTS AT EACH OCURRENCE OF SERIAL.PRINT) FOR CLARITY.

#if ( SERIAL_VERBOSE )
#define serial_console_print(x)     Serial.print(x)
#define serial_console_printdec(x)  Serial.print(x, DEC)
#define serial_console_println(x)   Serial.println(x)
#else
#define serial_console_print(x)
#define serial_console_printdec(x)
#define serial_console_println(x)
#endif


//MAVLINK Config
int MAVLINK_SYSTEM_ID = 1;                   ///< ID 20 for this airplane. 1 PX, 255 ground station
int MAVLINK_COMPONENT_ID = 158;                ///< The component sending the message
int MAVLINK_MAV_TYPE = MAV_TYPE_ONBOARD_CONTROLLER;   ///< This system is an airplane / fixed wing

uint8_t SYSTEM_TYPE = MAV_TYPE_GENERIC;
uint8_t MAVLINK_AUTOPILOT_TYPE = MAV_AUTOPILOT_INVALID;

uint8_t MAVLINK_SYSTEM_MODE = MAV_MODE_PREFLIGHT; ///< Booting up
uint32_t MAVLINK_CUSTOM_MODE = 0;                 ///< Custom mode, can be defined by user/adopter
uint8_t MAVLINK_SYSTEM_STATE = MAV_STATE_STANDBY; ///< System ready for flight

#define SATCOM_MAVLINK_HEARBEAT_PERIOD 30000

// Beacon config
#define HEARTBEAT_TIMEOUT  3000                           //Time (in milliseconds) before triggering beacon mode (default 10s = 10000ms)
#define BEACON_DISTRESS_MESSAGE_PERIOD  1000              //Time (in milliseconds) between distress messages     (default 60s = 60000ms)
#define RELAY_MESSAGE_PERIOD 1000                         //Time (in milliseconds) between relay messages        (default 60s = 60000ms)

// THIS SECTION DEALS WITH DEFINING THE PACKET DATA STRUCTURE
/*
 * In summary:
 * We basically want to send some data (a collection of diferent types, eg. floats, ints, etc) over a serial data link.
 * Naively, we could just cast each of whatever variables we want to send into a char array, and then concatenate for all
 * to obtain the binary packet. Then this would be unpacked in a similar way on the receiving end.
 * 
 * I propose a better solution using some obscure (obscure to me, im sure our coleages in CS would say they are pretty basic) C data types
 * struct and union.
 * 
 * While arrays can only hold data of the same kind (array of ints, array of floats), structures allow to 
 * combine data items of different kinds (variable types). Moreover, its entries can be conveniently accesed 
 * using dot notation (as oposed to index in an array). 
 * 
 * Note that the weird looking "__attribute__((__packed__))" is just to tell the compiler NOT to do packing on the 
 * struct while storing it in memory (more info: https://stackoverflow.com/questions/4306186/structure-padding-and-packing)
 * 
 * Now the union is slighly more interesting: It allows to store different data types in the same memory location (for 
 * example a float and an int, or, in this case, a struct and a char array.  This can be thought about the 
 * same object in memory having two aliases, one interpreted as a struct and one as a char array.
 * 
 * While a similar behavior could be implemented using pointer casting, this is much more readable and, in my opinion, easier 
 * to understand.
 * 
 * TLDR: We pack packet data into a struct called "data" (so for example we have data.latitude), then we create a union "message" with message.data and message.packet 
 * and send message.packet through the sat module.
 */

 //General comments: we could, in theory, just send actual mavlink messages
 //but the mavlink protocol has an overhead of 96 bits, already larger than the maximum native 
 //packet size of the sat radio, so clearly not viable.
 //HOWEVER, we might strip down the overhead and just send the payload if need be.

//Outgoing beacon packet structure
typedef struct __attribute__((__packed__)) satMessageBeaconData
{
  byte STX;                   // 1 byte (start message/end message)
  uint16_t bitmask1;          // 2 byte (for any IO settings)
  float GPS_lat;              // 4 byte 
  float GPS_long;             // 4 byte 
  float GPS_alt;              // 4 byte
  float SatVbatt;             // 4 byte (from Feather lipo) 
  uint8_t CurrentWaypoint;    // 1 byte (MAXIMUM 255 waypoints!) 
  uint8_t airspeed;           // 1 byte 
  float alt;                  // 4 byte
  float climb;                // 4 byte
  float heading;              // 4 byte (degress from North)
  uint8_t throttle;           // 1 byte (CH3%/pixhawk throttle out)
  uint16_t UAV_battery;       // 2 byte
  byte ETX;                   // 1 byte
  //                      TOTAL: 37 byte less than 50 OK
};
//Outgoing beacon packet union
typedef union satMessageBeaconPacket
{
  satMessageBeaconData data; //"alias 1": memory data to be interpreted as struct containing packet data
  uint8_t packet[PACKET_SIZE];  //"alias 2": memory data to be interpreted as binary packet
};


//Outgoing relay packet structure
typedef struct __attribute__((__packed__)) satMessageRelayData
{
  byte STX;                   // 1 byte (start message/end message)
  uint16_t bitmask1;          // 2 byte (for any IO settings)
  uint16_t bitmask2;          // 2 byte (for any IO settings)
  float GPS_lat;              // 4 byte 
  float GPS_long;             // 4 byte 
  float GPS_alt;              // 4 byte
  float SatVbatt;             // 4 byte (from Feather lipo) 
  uint16_t CurrentWaypoint;    // 1 byte (MAXIMUM 255 waypoints!) 
  float airspeed;             // 1 byte 
  float alt;                  // 4 byte
  float climb;                // 4 byte
  float heading;              // 4 byte (degress from North)
  float throttle;           // 1 byte (CH3%/pixhawk throttle out)
  float UAV_battery;       // 2 byte
  byte ETX;                   // 1 byte
  //                      TOTAL: 37 byte less than 50 OK
};
//Outgoing relay packet union
typedef union satMessageRelayPacket
{
  satMessageRelayData data; //"alias 1": memory data to be interpreted as struct containing packet data
  uint8_t packet[PACKET_SIZE];  //"alias 2": memory data to be interpreted as binary packet
};

// THIS SECTION DEALS WITH PIN DECLARATION. THIS IS A BIT HACKISH SINCE THE CURRENT IDE VESRION DOES NOT
// FULLY SUPPORT M0's FUNCTIONALITY YET.

// Arduino pin assignment (1/2)
//Config SERCOM1 as Serial3 on pins 11, 10 for Iridium
Uart Serial2 (&sercom1, 11, 10, SERCOM_RX_PAD_0, UART_TX_PAD_2);
void SERCOM1_Handler()
{
  Serial2.IrqHandler();
}

//Config SERCOM4 as Serial3 on pins MISO, MOSI for Pixhawk
Uart Serial3 (&sercom4, 22, 23, SERCOM_RX_PAD_0, UART_TX_PAD_2);
void SERCOM4_Handler()
{
  Serial3.IrqHandler();
}

// THIS SECTION DECLARES OBJECTS
// Sat modem
IridiumSBD modem(IridiumSerial);
// GPS
Adafruit_GPS GPS(&GPSSerial);
//Sat message
satMessageRelayPacket messageRelay;

// Mavlink variables
unsigned long previousMillisMAVLink = 0;                               // will store last time MAVLink was transmitted and listened

//Other variables
uint8_t err;                              // Used for interface with sat modem, taken as reply message. Make local?
int signalQuality = -1;               // Sat modem signal quality
float onboardVBatt = -1;              // Onboard battery (in V. Not equal to ADC readout!)

uint32_t Relay_Message_Timer = millis();
uint32_t Beacon_Distress_Message_Timer = millis();        // GPS activity timer
//uint32_t SatCommHEartbeat_timer = millis();        // Mavlink heartbeat timer
uint32_t Pixhawk_LastHeartbeatReceived_timer = millis();

//Initial setup (runs once)
void setup() {

  // Start the console serial port
  USBSerial.begin(115200);

  // (If selected) pause code until serial monitor opened on usb serial.
  #if WAIT_FOR_USB_CONNECTION
    while (!USBSerial) {};
    serial_console_println(("******************************"));
    serial_console_println(("UoB SatComm Mod alive.!"));
    serial_console_println(("******************************"));
  #endif


  // Start the serial port connected to the satellite modem
  IridiumSerial.begin(19200);
  // Start the serial port connected to GPS
  GPS.begin(9600);
  // Start the serial port connected to Pixhawk
  MAVLinkSerial.begin(57600);

  // THIS SECTION DEALS WITH PIN DECLARATION. THIS IS A BIT HACKISH SINCE THE CURRENT IDE VESRION DOES NOT
  // FULLY SUPPORT M0's FUNCTIONALITY YET.
  //Arduino pin assignment (2/2)
  // Assign pins 10 & 11 SERCOM1 functionality (Seria2)
  pinPeripheral(10, PIO_SERCOM);  //TX (10)
  pinPeripheral(11, PIO_SERCOM);  //RX (11)
  // Assign pins MISO & MOSI SERCOM4 functionality (Serial3)
  pinPeripheral(22, PIO_SERCOM_ALT); //RX (MISO on board)
  pinPeripheral(23, PIO_SERCOM_ALT); //TX (MOSI on board)

  // Begin satellite modem operation
  serial_console_println("Starting modem...");
  err = modem.begin();
  if (err != ISBD_SUCCESS)
  {
    serial_console_println("Begin failed: error ");
    serial_console_println(err);
    if (err == ISBD_NO_MODEM_DETECTED)
      serial_console_println("No modem detected: check wiring.");
    return;
  }
  
  //Set up satellite modem
  modem.setPowerProfile(IridiumSBD::USB_POWER_PROFILE); //Low power, 90ma, T>60s
  //modem.setPowerProfile(IridiumSBD::DEFAULT_POWER_PROFILE); //High power, 450ma, T>20s
  //Exprimental
  //modem.adjustATTimeout(20);          // default value = 20 seconds
  //modem.adjustSendReceiveTimeout(300); // default value = 300 seconds
  
  // Set up GPS
  GPS.sendCommand(PMTK_SET_NMEA_OUTPUT_RMCGGA); // turn on RMC + GGA (fix data) NMEA sentence
  GPS.sendCommand(PMTK_SET_NMEA_UPDATE_1HZ);    // GPS update rate (Up to 10Hz)


  #if SERIAL_VERBOSE
    // Print the sat modem firmware version 
    char version[12];
    err = modem.getFirmwareVersion(version, sizeof(version));
    if (err != ISBD_SUCCESS)
    {
      serial_console_println("Sat Modem FirmwareVersion failed: error ");
      serial_console_println(err);
      return;
    }
    serial_console_println("Sat Modem Firmware Version is ");
    serial_console_println(version);
    serial_console_println(".");
  
    // Print the GPS firmware version.
    serial_console_print("GPS firmware version ");
    serial_console_println(PMTK_Q_RELEASE);
    serial_console_println("GPS OK!");
  #endif  

  // ############   STARTUP DELAY   ###########
  delay(5000);
  // ############   STARTUP DELAY   ###########

  Pixhawk_LastHeartbeatReceived_timer = millis(); //Reset pixhawk timeout 
}

void loop() {

  // READ GPS DATA
  char c = GPS.read();

  #if GPS_NMEA_ECHO
    serial_console_print(c);
  #endif

  if (GPS.newNMEAreceived()) // If new NMEA availiable, parse it.
  {
    GPS.parse(GPS.lastNMEA());
  }

  //PACK FEATHER GPS ONTO MSG
  messageRelay.data.GPS_lat = GPS.latitude;
  messageRelay.data.GPS_long = GPS.longitude;
  messageRelay.data.GPS_alt = GPS.altitude;

  //PACK BATT DATA ONTO MSG
  messageRelay.data.SatVbatt = getVBatt();

  //CHECK PIXHAWK FAILURE (NO HEARTBEAT RECEIVED)
  //if millis() or timer wraps around, reset it (Do we really need to care about mills overflow? - usually 50 days for uint32_t datatype)
  if (Pixhawk_LastHeartbeatReceived_timer > millis()) Pixhawk_LastHeartbeatReceived_timer = millis();

  //The first timer looks for how long it has been since we recived a Pixhawk heartbeat, the second timer controls how often we then send a distress message. This
  //allows a change in sat comms rate between nortmal operation and distress operation. 
  if (millis() - Pixhawk_LastHeartbeatReceived_timer > HEARTBEAT_TIMEOUT) {
    
    // if millis() or timer wraps around, reset it (Do we really need to care about mills overflow? - usually 50 days for uint32_t datatype)
    if (Beacon_Distress_Message_Timer > millis()) Beacon_Distress_Message_Timer = millis();
    if (millis() - Beacon_Distress_Message_Timer > BEACON_DISTRESS_MESSAGE_PERIOD)
    {
      //Reset timer
      Beacon_Distress_Message_Timer = millis();
      //Indicate no pixhawk connection
      serial_console_println("Bitmask set to 0: No connected");
      messageRelay.data.bitmask1 = 0;
      
      messageRelay.data.STX = 0x02;
      messageRelay.data.ETX = 0x03;
      USBSerial.write(messageRelay.packet,PACKET_SIZE);
      serial_console_println("Distress message sent");

      //NOTE: Satcom send can take upto 45-60 seconds.
      //TODO: should be a satcomm send here
    }
  }
  else //pixhawk is good
  {
    //Deal with millis() wrap around
    if (Relay_Message_Timer > millis()) Relay_Message_Timer = millis();

    //If the timer for normal operation mode times out, send a normal staus message
    if (millis() - Relay_Message_Timer > RELAY_MESSAGE_PERIOD)
    {
      //Reset timer
      Relay_Message_Timer = millis();
      //Indicate availiable pixhawk connection
      serial_console_println("Bitmask set to 1: Connected");
      messageRelay.data.bitmask1 = 1;

      //TODO: these bytes should be set first not last.
      messageRelay.data.STX = 0x02;
      messageRelay.data.ETX = 0x03;
      USBSerial.write(messageRelay.packet,PACKET_SIZE);
      serial_console_println("Relay message sent");

      //NOTE: Satcom send can take upto 45-60 seconds.
      //TODO: should be a satcomm send here
    }
  }


  //This section of code deals with the functions to request datastreams from the Pixhawk.
  //The datastream is requested periodically.
  unsigned long currentMillisMAVLink = millis();
  if (currentMillisMAVLink - previousMillisMAVLink >= SATCOM_MAVLINK_HEARBEAT_PERIOD) {
    previousMillisMAVLink = currentMillisMAVLink;

    // Request streams from Pixhawk
    serial_console_println("Streams requested!");
    Mav_Request_Data();    
  }

  // Check reception buffer. This is as fast as possible to collect all MAVLink data from the Pixahawk.
  comm_receive();
}

/*
void Mav_Send_Heartbeat()
{
    //DEPRECATED
  // Initialize msg buffer
  mavlink_message_t msg;
  uint8_t buf[MAVLINK_MAX_PACKET_LEN];
  
  // Pack the heartbeat message
  //mavlink_msg_heartbeat_pack(MAVLINK_SYSTEM_ID, MAVLINK_COMPONENT_ID, &msg, MAVLINK_MAV_TYPE, MAVLINK_AUTOPILOT_TYPE, MAVLINK_SYSTEM_MODE, MAVLINK_CUSTOM_MODE, MAVLINK_SYSTEM_STATE);
   mavlink_msg_heartbeat_pack(1, 0, &msg, MAVLINK_MAV_TYPE, MAVLINK_AUTOPILOT_TYPE, MAVLINK_SYSTEM_MODE, MAVLINK_CUSTOM_MODE, MAVLINK_SYSTEM_STATE);
  
  // Copy the heartbeat message to the send buffer, get lenght
  uint16_t len = mavlink_msg_to_send_buffer(buf, &msg);
  
  // Send the heartbeat message though serial
  //MAVLinkSerial.write(buf, len);
}
*/

void Mav_Request_Data()
{
  mavlink_message_t msg;
  uint8_t buf[MAVLINK_MAX_PACKET_LEN];

  const int  maxStreams = 3;
  const uint8_t MAVStreams[maxStreams] = {MAV_DATA_STREAM_EXTENDED_STATUS, MAV_DATA_STREAM_EXTRA1, MAV_DATA_STREAM_EXTRA2};
  const uint16_t MAVRates[maxStreams] = {0x02,0x05,0x05};

  //MAV_DATA_STREAM_ALL
  for (int i=0; i < maxStreams; i++)
  {
    mavlink_msg_request_data_stream_pack(2, 200, &msg, 1, 0, MAVStreams[i], MAVRates[i], 1);
    uint16_t len = mavlink_msg_to_send_buffer(buf, &msg);

    MAVLinkSerial.write(buf,len);
  }
}

void comm_receive() {
 
  mavlink_message_t msg;
  mavlink_status_t status;

  while(MAVLinkSerial.available()>0)
  {
    uint8_t c = MAVLinkSerial.read();

    serial_console_println("Incoming  PX Character");
    // Try to get a new message
    if(mavlink_parse_char(MAVLINK_COMM_0, c, &msg, &status)) {
    serial_console_println("Complete msg received  PX");
      // Handle message
      switch(msg.msgid) {
        case MAVLINK_MSG_ID_HEARTBEAT:  // #0: Heartbeat
          {
            // E.g. read GCS heartbeat and go into
            // comm lost mode if timer times out
            Pixhawk_LastHeartbeatReceived_timer = millis();
          }
          break;

        case MAVLINK_MSG_ID_SYS_STATUS:  // #1: SYS_STATUS
          {
            /* Message decoding: PRIMITIVE
             *    mavlink_msg_sys_status_decode(const mavlink_message_t* msg, mavlink_sys_status_t* sys_status)
             */
            //mavlink_message_t* msg;
            mavlink_sys_status_t sys_status;
            mavlink_msg_sys_status_decode(&msg, &sys_status);
            
            messageRelay.data.UAV_battery = sys_status.voltage_battery;          
          }
          break;

        case MAVLINK_MSG_ID_PARAM_VALUE:  // #22: PARAM_VALUE
          {
            /* Message decoding: PRIMITIVE
             *    mavlink_msg_param_value_decode(const mavlink_message_t* msg, mavlink_param_value_t* param_value)
             */
            //mavlink_message_t* msg;
            mavlink_param_value_t param_value;
            mavlink_msg_param_value_decode(&msg, &param_value);

            //TODO: Could choose to decode a requested parameter value here.
          }
          break;

        case MAVLINK_MSG_ID_MISSION_CURRENT:  // #42: MISSION_CURRENT
          {
            /* Message decoding: PRIMITIVE
             *    mavlink_msg_mission_current_decode(const mavlink_message_t* msg, mavlink_param_value_t* param_value)
             */
            //mavlink_message_t* msg;
            mavlink_mission_current_t mission_current;
            mavlink_msg_mission_current_decode(&msg, &mission_current);

            messageRelay.data.CurrentWaypoint = mission_current.seq;
          }
          break;

        case MAVLINK_MSG_ID_RAW_IMU:  // #27: RAW_IMU
          {
            /* Message decoding: PRIMITIVE
             *    static inline void mavlink_msg_raw_imu_decode(const mavlink_message_t* msg, mavlink_raw_imu_t* raw_imu)
             */
            mavlink_raw_imu_t raw_imu;
            mavlink_msg_raw_imu_decode(&msg, &raw_imu);

            //TODO: could get aircraft orientation here if required.

          }
          break;

        case MAVLINK_MSG_ID_VFR_HUD:  // #74: VFR_HUD
          {
            /* Message decoding: PRIMITIVE
             *    static inline void mavlink_msg_vfr_hud_decode(const mavlink_message_t* msg, mavlink_vfr_hud_t* vfr_hud)
             */
            mavlink_vfr_hud_t vfr_hud;
            mavlink_msg_vfr_hud_decode(&msg, &vfr_hud);

            messageRelay.data.airspeed = vfr_hud.airspeed;
            messageRelay.data.alt = vfr_hud.alt;
            messageRelay.data.climb = vfr_hud.climb;
            messageRelay.data.heading = vfr_hud.heading;
            messageRelay.data.throttle = vfr_hud.throttle;
          }
          break;

        case MAVLINK_MSG_ID_ATTITUDE:  // #30
          {
            /* Message decoding: PRIMITIVE
             *    mavlink_msg_attitude_decode(const mavlink_message_t* msg, mavlink_attitude_t* attitude)
             */
            mavlink_attitude_t attitude;
            mavlink_msg_attitude_decode(&msg, &attitude);

            //TODO: could get aicraft attitude here.
          }
          break;

       default:
          break;
      }
    }
  }
}

//Sample the analogue pin to 
float getVBatt()
{
  float VBatt = -1;
  VBatt = ADCMULTIPLIER_VBAT * analogRead(PIN_VBATT_INTERNAL);
  return VBatt;
}

#if IRIDIUM_DIAGNOSTICS
void ISBDConsoleCallback(IridiumSBD *device, char c)
{
  USBSerial.write(c);
}

void ISBDDiagsCallback(IridiumSBD *device, char c)
{
  USBSerial.write(c);
}
#endif
