Code for a satellite communications module using:
Feather M0 Basic Proto
Adafruit Ultimate GPS FeatherWing
RockBLOCK 9603

Need to configure Arduino IDE to work with the Feather
https://learn.adafruit.com/add-boards-arduino-v164/setup
https://learn.adafruit.com/adafruit-feather-m0-basic-proto/using-with-arduino-ide

Install Anaconda Python 2 into the ProgramData directory. If the .bat script complains of missing
package future then run:
C:\ProgramData\Anaconda2\Scripts\pip.exe install future

Use:
git submodule update --init --recursive
to checkout the MAVLink code.

Use:
generate_mavlink.bat
to create the required C++ MAVLink headers

